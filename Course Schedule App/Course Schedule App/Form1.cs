﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Course_Schedule_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int finalsButtonCounter;
        private List<Scheduler> scheduler = new List<Scheduler>();

        private int currentScheduleIndex;

        private List<course> searchResults = new List<course>();
        private List<course> addedCourses = new List<course>();
        private List<Button> results = new List<Button>();
        private List<course> conflicts = new List<course>();
        private List<Button> addButtons = new List<Button>();
        private List<Button> removeButtons = new List<Button>();

        // These lists hold the buttons for the calendar view for each day
        private List<Button> Monday = new List<Button>();
        private List<Button> Tuesday = new List<Button>();
        private List<Button> Wednesday = new List<Button>();
        private List<Button> Thursday = new List<Button>();
        private List<Button> Friday = new List<Button>();
        private List<Button> savedCourses = new List<Button>(); // saves the buttons that represent courses in the calendar view

        // These lists hold the finals buttons
        private List<Button> finalsMonday = new List<Button>();
        private List<Button> finalsTuesday = new List<Button>();
        private List<Button> finalsSaturday = new List<Button>();
        private List<Button> finalsThursday = new List<Button>();
        private List<Button> finalsFriday = new List<Button>();
        private List<Button> savedFinals = new List<Button>(); // saves the buttons for the finals

        // Holds the colors for the buttons
        private List<Color> blockColors = new List<Color>();
        private List<Button> scheduleButtonsList = new List<Button>();
        private List<Button> exitScheduleButtonsList = new List<Button>();
        private Color conflict = Color.Coral;
        private List<Button> AddedCoursesButtonList = new List<Button>();



        private void Form1_Load(object sender, EventArgs e)
        {

            finalsButtonCounter = 0;

            Left = 125;
            Top = 25;

            search_results_panel.AutoScroll = true;
            added_courses_panel.AutoScroll = true;
            //This is the first schedule the will be passed into the scheduler list (will be the initial schedule shown to the user)
            Scheduler StarterSchedule = new Scheduler();
            SortedSet<string> departments = new SortedSet<string>();
            SortedSet<string> codes = new SortedSet<string>();

            foreach (course c in StarterSchedule.allCourses)
            {
                departments.Add(c.Department.ToString());
                codes.Add(c.CourseNumber.ToString());
            }

            departmentBox.Items.Add("");
            courseCodeBox.Items.Add("");
            //store start up 
            scheduler.Add(StarterSchedule);
            //sets the current schedule in use to the startup schedule
            currentScheduleIndex = 0;

            foreach (string s in departments) {
                departmentBox.Items.Add(s);
            }

            foreach (string s in codes)
            {
                courseCodeBox.Items.Add(s);
            }

            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;

            #region Add Buttons

            Monday.Add(M8A);
            Monday.Add(M9A);
            Monday.Add(M10A);
            Monday.Add(M11A);
            Monday.Add(M12P);
            Monday.Add(M1P);
            Monday.Add(M2P);
            Monday.Add(M3P);
            Monday.Add(M4P);
            Monday.Add(M5P);
            Monday.Add(M6P);

            Wednesday.Add(W8A);
            Wednesday.Add(W9A);
            Wednesday.Add(W10A);
            Wednesday.Add(W11A);
            Wednesday.Add(W12P);
            Wednesday.Add(W1P);
            Wednesday.Add(W2P);
            Wednesday.Add(W3P);
            Wednesday.Add(W4P);
            Wednesday.Add(W5P);
            Wednesday.Add(W6P);

            Friday.Add(F8A);
            Friday.Add(F9A);
            Friday.Add(F10A);
            Friday.Add(F11A);
            Friday.Add(F12P);
            Friday.Add(F1P);
            Friday.Add(F2P);
            Friday.Add(F3P);
            Friday.Add(F4P);
            Friday.Add(F5P);
            Friday.Add(F6P);

            Tuesday.Add(T8A);
            Tuesday.Add(T10A);
            Tuesday.Add(T11A);
            Tuesday.Add(T1P);
            Tuesday.Add(T2P);
            Tuesday.Add(T4P);
            Tuesday.Add(T6P);

            Thursday.Add(R8A);
            Thursday.Add(R10A);
            Thursday.Add(R11A);
            Thursday.Add(R1P);
            Thursday.Add(R2P);
            Thursday.Add(R4P);
            Thursday.Add(R6P);

            finalsMonday.Add(FM2);
            finalsMonday.Add(FM7);
            finalsMonday.Add(FM9);

            finalsTuesday.Add(FT2);
            finalsTuesday.Add(FT7);
            finalsTuesday.Add(FT9);

            finalsSaturday.Add(FS2);
            finalsSaturday.Add(FS7);
            finalsSaturday.Add(FS9);

            finalsFriday.Add(FF2);
            finalsFriday.Add(FF7);
            finalsFriday.Add(FF9);

            finalsThursday.Add(FR7);

            scheduleButtonsList.Add(Schedule1_Button);
            scheduleButtonsList.Add(Schedule2_Button);
            scheduleButtonsList.Add(Schedule3_Button);
            scheduleButtonsList.Add(Schedule4_Button);
            scheduleButtonsList.Add(Schedule5_Button);
            scheduleButtonsList.Add(Schedule6_Button);

            exitScheduleButtonsList.Add(Schedule1_EXIT_Button);
            exitScheduleButtonsList.Add(Schedule2_EXIT_Button);
            exitScheduleButtonsList.Add(Schedule3_EXIT_Button);
            exitScheduleButtonsList.Add(Schedule4_EXIT_Button);
            exitScheduleButtonsList.Add(Schedule5_EXIT_Button);
            exitScheduleButtonsList.Add(Schedule6_EXIT_Button);

            #endregion

            scheduleButtonsList[0].BackColor = Color.AliceBlue;

            #region Add Colors
            blockColors.Add(Color.AliceBlue);
            blockColors.Add(Color.Bisque);
            blockColors.Add(Color.LavenderBlush);
            blockColors.Add(Color.Aquamarine);
            blockColors.Add(Color.Lavender);
            blockColors.Add(Color.CornflowerBlue);
            blockColors.Add(Color.SeaShell);
            blockColors.Add(Color.MintCream);

            // Color.Coral reserved for conflicts
            #endregion



            loadButtons();
        }

        private void loadButtons() {
            //  departmentButton.FlatAppearance = FlatButtonAppearance
        }

        private void changeLabels(bool finals) // Changes the headers for the days of the calendar view
        {
            int currentLocation = 0; // for recentering the labels when the text changes

            if (finals) // if finals week is selected...
            {
                mondayLabel.Text = "Thursday";
                currentLocation = mondayLabel.Location.X;
                mondayLabel.Location = new Point(currentLocation - 7, mondayLabel.Location.Y);

                tuesdayLabel.Text = "Friday";
                currentLocation = tuesdayLabel.Location.X;
                tuesdayLabel.Location = new Point(currentLocation + 7, tuesdayLabel.Location.Y);

                wednesdayLabel.Text = "Saturday";
                currentLocation = wednesdayLabel.Location.X;
                wednesdayLabel.Location = new Point(currentLocation + 11, wednesdayLabel.Location.Y);

                thursdayLabel.Text = "Monday";
                currentLocation = thursdayLabel.Location.X;
                thursdayLabel.Location = new Point(currentLocation + 4, thursdayLabel.Location.Y);

                fridayLabel.Text = "Tuesday";
                currentLocation = fridayLabel.Location.X;
                fridayLabel.Location = new Point(currentLocation - 10, fridayLabel.Location.Y);
            }

            else
            {
                mondayLabel.Text = "Monday";
                currentLocation = mondayLabel.Location.X;
                mondayLabel.Location = new Point(currentLocation + 7, mondayLabel.Location.Y);

                tuesdayLabel.Text = "Tuesday";
                currentLocation = tuesdayLabel.Location.X;
                tuesdayLabel.Location = new Point(currentLocation - 7, tuesdayLabel.Location.Y);

                wednesdayLabel.Text = "Wednesday";
                currentLocation = wednesdayLabel.Location.X;
                wednesdayLabel.Location = new Point(currentLocation - 11, wednesdayLabel.Location.Y);

                thursdayLabel.Text = "Thursday";
                currentLocation = thursdayLabel.Location.X;
                thursdayLabel.Location = new Point(currentLocation - 4, thursdayLabel.Location.Y);

                fridayLabel.Text = "Friday";
                currentLocation = fridayLabel.Location.X;
                fridayLabel.Location = new Point(currentLocation + 10, fridayLabel.Location.Y);
            }
        }

        #region CalendarFunctions

        private void calendarUpdate()
        {
            int colorCounter = 0;

            if (!finalsButtonSmall.Checked)
            {
                if (savedCourses.Count != scheduler[currentScheduleIndex].scheduledCourses.Count)
                {

                    foreach (course c in scheduler[currentScheduleIndex].scheduledCourses)
                    {
                        int slot = findSlot(c.Day, c.BeginTime);

                        if (c.Day.Contains("M"))
                        {
                            Monday[slot].Text = c.CourseCodeInfo;
                            Monday[slot].Visible = true;
                            Monday[slot].BackColor = blockColors[colorCounter];
                            savedCourses.Add(Monday[slot]);
                        }

                        if (c.Day.Contains("T"))
                        {
                            Tuesday[slot].Text = c.CourseCodeInfo;
                            Tuesday[slot].Visible = true;
                            Tuesday[slot].BackColor = blockColors[colorCounter];
                            savedCourses.Add(Tuesday[slot]);
                        }

                        if (c.Day.Contains("W"))
                        {
                            Wednesday[slot].Text = c.CourseCodeInfo;
                            Wednesday[slot].Visible = true;
                            Wednesday[slot].BackColor = blockColors[colorCounter];
                            savedCourses.Add(Wednesday[slot]);
                        }

                        if (c.Day.Contains("R"))
                        {
                            Thursday[slot].Text = c.CourseCodeInfo;
                            Thursday[slot].Visible = true;
                            Thursday[slot].BackColor = blockColors[colorCounter];
                            savedCourses.Add(Thursday[slot]);
                        }

                        if (c.Day.Contains("F"))
                        {
                            Friday[slot].Text = c.CourseCodeInfo;
                            Friday[slot].Visible = true;
                            Friday[slot].BackColor = blockColors[colorCounter];
                            savedCourses.Add(Friday[slot]);
                        }

                        if (colorCounter < scheduler[currentScheduleIndex].scheduledCourses.Count && colorCounter < blockColors.Count - 1) colorCounter++;
                        else colorCounter = 0;
                    }
                }
                else
                {
                    foreach (Button b in savedCourses)
                    {
                        b.Visible = true;
                    }
                }

                conflictUpdate();
            }

            else
            {
                clearFinals();
                calendarUpdate_Finals();
            }
        }


        private void calendarUpdate_Finals()
        {
            int colorCounter = 0;
            int slot = 0;

            if (finalsButtonSmall.Checked)
            {

                clearCalendar();
                clearFinals();
                savedFinals.Clear();

                foreach (course c in scheduler[currentScheduleIndex].scheduledCourses)
                {
                    slot = findFinalsSlot(c.Finals);
                    if (slot < 0 || slot > 2) // checks if slot gets correct info
                        continue;


                    if (c.FinalsDay == "R")
                    {// use 0 because there is only one possibility for Thursday

                        if (conflicts.Exists(x => x.Day == c.Day && x.BeginString == c.BeginString)) { displayConflict(c, finalsThursday, 0); }

                        else displayFinalsBlock(c, finalsThursday, 0, colorCounter);
                    }

                    else if (c.FinalsDay == "F")
                    {
                        if (conflicts.Exists(x => x.Day == c.Day && x.BeginString == c.BeginString)) { displayConflict(c, finalsFriday, slot); }

                        else displayFinalsBlock(c, finalsFriday, slot, colorCounter);
                    }

                    else if (c.FinalsDay == "S")
                    {
                        if (conflicts.Exists(x => x.Day == c.Day && x.BeginString == c.BeginString)) { displayConflict(c, finalsSaturday, slot); }

                        else displayFinalsBlock(c, finalsSaturday, slot, colorCounter);
                    }

                    else if (c.FinalsDay == "M")
                    {
                        if (conflicts.Exists(x => x.Day == c.Day && x.BeginString == c.BeginString)) { displayConflict(c, finalsMonday, slot); }

                        else displayFinalsBlock(c, finalsMonday, slot, colorCounter);
                    }

                    else if (c.FinalsDay == "T")
                    {
                        if (conflicts.Exists(x => x.Day == c.Day && x.BeginString == c.BeginString)) { displayConflict(c, finalsTuesday, slot); }

                        else displayFinalsBlock(c, finalsTuesday, slot, colorCounter);
                    }

                    if (colorCounter < scheduler[currentScheduleIndex].scheduledCourses.Count && colorCounter < blockColors.Count - 1) colorCounter++;
                    else colorCounter = 0;
                }
            }
            else return;
        }

        private void displayFinalsBlock(course c, List<Button> day, int index, int color)
        {
            day[index].Text = c.CourseCodeInfo;
            day[index].Visible = true;
            day[index].BackColor = blockColors[color];
            savedFinals.Add(day[index]);
        }

        private int findFinalsSlot(TimeSpan finalsTime)
        { // converts the finals time to a time slot button for the calendar view

            if (finalsTime == new TimeSpan(9, 0, 0)) return 0;
            else if (finalsTime == new TimeSpan(14, 0, 0)) return 1;
            else if (finalsTime == new TimeSpan(19, 0, 0)) return 2;
            else return -1;
        }

        private void clearCalendar() // clears the regular course buttons to show the finals schedule
        {
            foreach (Button b in savedCourses)
            {
                b.Visible = false;
            }
        }

        private void clearCalender()
        {
            foreach (Button currButton in Monday)
                currButton.Visible = false;

            foreach (Button currButton in Tuesday)
                currButton.Visible = false;

            foreach (Button currButton in Wednesday)
                currButton.Visible = false;

            foreach (Button currButton in Thursday)
                currButton.Visible = false;

            foreach (Button currButton in Friday)
                currButton.Visible = false;

            foreach (Button currButton in removeButtons)
                currButton.Visible = false;

            addedCourses.Clear();
        }

        private void displayConflict(course c, List<Button> day, int index)
        {
            List<course> conflictions = new List<course>(); // temporary list for holding all conflicts during that time

            foreach (course conflicted in conflicts)
            {
                course temp = c;
                if (conflicted.Finals == c.Finals && conflicted.FinalsDay == c.FinalsDay && c.CourseCodeInfo != conflicted.CourseCodeInfo)
                {
                    temp = conflicted;
                    conflictions.Add(temp);
                }
            }

            day[index].Text = "Conflict: " + c.CourseCodeInfo + "\n";

            if (conflictions.Count > 1)
            {
                day[index].Text += conflictions[0].CourseCodeInfo;
                day[index].Text += "...";
            }

            else
            {
                foreach (course conflicted in conflictions)
                {
                    day[index].Text += conflicted.CourseCodeInfo + "\n";
                }

            }

            day[index].Visible = true;
            day[index].BackColor = conflict;
            savedFinals.Add(day[index]);
        }

        private void conflictUpdate()
        {
            conflicts = Tester.detectConflicts(scheduler[currentScheduleIndex].scheduledCourses);

            if (!finalsButtonSmall.Checked)
            {

                foreach (course c in conflicts)
                {
                    int slot = findSlot(c.Day, c.BeginTime);

                    if (c.Day.Contains("M"))
                    {
                        displayConflict(c, Monday, slot);
                    }

                    if (c.Day.Contains("T"))
                    {
                        displayConflict(c, Tuesday, slot);
                    }

                    if (c.Day.Contains("W"))
                    {
                        displayConflict(c, Wednesday, slot);
                    }

                    if (c.Day.Contains("R"))
                    {
                        displayConflict(c, Thursday, slot);
                    }

                    if (c.Day.Contains("F"))
                    {
                        displayConflict(c, Friday, slot);
                    }
                }
            }
            else calendarUpdate_Finals();
        }

        private void calendarRemoveUpdate(course c)
        {
            int slot = findSlot(c.Day, c.BeginTime);

            if (c.Day.Contains("M"))
            {
                Monday[slot].Visible = false;
            }

            if (c.Day.Contains("T"))
            {
                Tuesday[slot].Visible = false;
            }

            if (c.Day.Contains("W"))
            {
                Wednesday[slot].Visible = false;
            }

            if (c.Day.Contains("R"))
            {
                Thursday[slot].Visible = false;
            }

            if (c.Day.Contains("F"))
            {
                Friday[slot].Visible = false;
            }
        }

        private int findSlot(string day, TimeSpan time)
        {
            int slot = -1;
            if (day.Contains("M") || day.Contains("W") || day.Contains("F"))
            {
                if (time >= TimeSpan.Parse("8:00:00") && time < TimeSpan.Parse("9:00:00"))
                {
                    return 0;
                }
                if (time >= TimeSpan.Parse("9:00:00") && time < TimeSpan.Parse("10:00:00"))
                {
                    return 1;
                }
                if (time >= TimeSpan.Parse("10:00:00") && time < TimeSpan.Parse("11:00:00"))
                {
                    return 2;
                }
                if (time >= TimeSpan.Parse("11:00:00") && time < TimeSpan.Parse("12:00:00"))
                {
                    return 3;
                }
                if (time >= TimeSpan.Parse("12:00:00") && time < TimeSpan.Parse("13:00:00"))
                {
                    return 4;
                }
                if (time >= TimeSpan.Parse("13:00:00") && time < TimeSpan.Parse("14:00:00"))
                {
                    return 5;
                }
                if (time >= TimeSpan.Parse("14:00:00") && time < TimeSpan.Parse("15:00:00"))
                {
                    return 6;
                }
                if (time >= TimeSpan.Parse("15:00:00") && time < TimeSpan.Parse("16:00:00"))
                {
                    return 7;
                }
                if (time >= TimeSpan.Parse("16:00:00") && time < TimeSpan.Parse("17:00:00"))
                {
                    return 8;
                }
                if (time >= TimeSpan.Parse("17:00:00") && time < TimeSpan.Parse("18:30:00"))
                {
                    return 9;
                }
                if (time >= TimeSpan.Parse("18:30:00"))
                {
                    return 10;
                }



            }
            else if (day.Contains("T") || day.Contains("R"))
            {
                if (time >= TimeSpan.Parse("8:00:00") && time < TimeSpan.Parse("9:15:00"))
                {
                    return 0;
                }
                if (time >= TimeSpan.Parse("9:15:00") && time < TimeSpan.Parse("11:20:00"))
                {
                    return 1;
                }
                if (time >= TimeSpan.Parse("11:20:00") && time < TimeSpan.Parse("12:45:00"))
                {
                    return 2;
                }
                if (time >= TimeSpan.Parse("12:45:00") && time < TimeSpan.Parse("14:30:00"))
                {
                    return 3;
                }
                if (time >= TimeSpan.Parse("14:30:00") && time < TimeSpan.Parse("15:45:00"))
                {
                    return 4;
                }
                if (time >= TimeSpan.Parse("15:45:00") && time < TimeSpan.Parse("18:00:00"))
                {
                    return 5;
                }
                if (time >= TimeSpan.Parse("18:00:00"))
                {
                    return 6;
                }

            }
            return slot;
        }

        #endregion

        #region SearchFunctions

        private void searchButton_Click(object sender, EventArgs e)
        {
            TimeSpan addReference = new TimeSpan(12, 00, 00);
            bool emptySearch = false;
            bool[] checkedItems = new bool[5]; // For days of the week
            string tstart = "";
            string courseNumber = "";
            string title = "";
            string department = "";
            object tempTimePicker = timePicker.SelectedItem;
            object tempCourseCode = courseCodeBox.SelectedItem;
            object tempDepartment = departmentBox.SelectedItem;
            search_results_panel.Controls.Clear();

            // For UI dropdown boxes:
            if (departmentBox.Text == String.Empty)
                departmentLabel.Visible = true;

            if (courseCodeBox.Text == String.Empty)
                courseCodeLabel.Visible = true;

            if (tempCourseCode != null)
                courseNumber = tempCourseCode.ToString();
            if (tempDepartment != null)
                department = tempDepartment.ToString();
            if (searchBox.Text.ToString() != "Course Name" && searchBox.Text.ToString() != "")
                title = searchBox.Text.ToString();
            for (int i = 0; i < checkedItems.Length; i++)
                checkedItems[i] = false;
            foreach (int index in dayPicker.CheckedIndices)
                checkedItems[index] = true;

            if (tempTimePicker != "" && tempTimePicker != null)
            {
                tstart = tempTimePicker.ToString() + ":00";
                TimeSpan toConvert = TimeSpan.Parse(tstart);
                if (toConvert >= TimeSpan.Parse("1:00:00") && toConvert < TimeSpan.Parse("7:00:00"))
                {
                    toConvert += addReference;
                    tstart = toConvert.ToString();
                }
            }

            if ((tstart == "") &&
                (courseNumber == "") &&
                (title == "") &&
                (department == "") &&
                (tempTimePicker == null) &&
                (!checkedItems.Contains<bool>(true)))
            {
                emptySearch = true;
            }

            if (!emptySearch)
            {
                searchResults = Search.fullSearch(tstart, 
                                                  checkedItems,    
                                                  courseNumber, 
                                                  title, 
                                                  department, 
                                                  ref scheduler[currentScheduleIndex].allCourses);
                populateSearchPanel();
            }


        }

        /// <summary>
        /// Generates all of the buttons following a searcn call.
        /// </summary>
        void populateSearchPanel()
        {
            int buttonTracker = 0;
            foreach (course c in searchResults)
            {
                Result result = new Result();
                result.ElementNumber = buttonTracker;
                result.Click += new EventHandler(search_result_click);
                result.Text = c.DisplayString;
                result.FlatStyle = FlatStyle.Flat;
                result.BackColor = Color.WhiteSmoke;
                result.Width = 243;
                result.Height = 40;
                search_results_panel.Controls.Add(result);
                buttonTracker++;
            }
        }

        /// <summary>
        /// Onclick event for search result buttons. Adds the course and performs calendar updates.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void search_result_click(object sender, EventArgs e)
        {
            Result result = sender as Result;
            course selectedCourse = searchResults[result.ElementNumber];
            if (!scheduler[currentScheduleIndex].scheduledCourses.Contains(selectedCourse))
                scheduler[currentScheduleIndex].scheduledCourses.Add(selectedCourse);
            calendarUpdate();
            updateAddedCourses();
            conflictUpdate();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                searchButton.Click += new EventHandler(searchButton_Click);
                searchButton_Click(sender, e);
            }
        }

        #endregion

        #region AddedCourses

        /// <summary>
        /// Updates the panel containing added courses.
        /// </summary>
        void updateAddedCourses()
        {
            int buttonNumber = 0;
            added_courses_panel.Controls.Clear();
            foreach (course c in scheduler[currentScheduleIndex].scheduledCourses)
            {
                Result result = new Result();

                result.ElementNumber = buttonNumber;
                result.Click += new EventHandler(added_course_click);
                result.Text += c.DisplayString;
                result.FlatStyle = FlatStyle.Flat;
                result.BackColor = Color.WhiteSmoke;
                if (conflicts.Exists(a => a.CourseCodeInfo == c.CourseCodeInfo && a.Day == c.Day))
                {
                    result.Text = "Conflict: " + c.DisplayString;
                    result.BackColor = conflict;
                }

                result.Width = 243;
                result.Height = 40;
                added_courses_panel.Controls.Add(result);
                buttonNumber++;
            }
        }

        /// <summary>
        /// Removes the clicked course from added courses, and performs a view update.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void added_course_click(object sender, EventArgs e)
        {
            Result result = sender as Result;
            course toRemove = scheduler[currentScheduleIndex].scheduledCourses[result.ElementNumber];
            calendarRemoveUpdate(toRemove);
            scheduler[currentScheduleIndex].scheduledCourses.RemoveAt(result.ElementNumber);
            conflictUpdate();
            updateAddedCourses();
            calendarUpdate();
        }
        #endregion

        #region FinalsSchedule

        private void clearFinals()
            {
                foreach (Button b in savedFinals)
                {
                    b.Visible = false;
                }
            }
    
        #endregion

        #region MultipleScheduleFunctions
            //new functions for multiple schedules
            private void resetScheduleButtons()
            {
                if (scheduler.Count == 0)
                {
                    scheduler.Add(new Scheduler());
                }
                else
                {
                    for (int i = 0; i < 6; i++)
                    {
                        scheduleButtonsList[i].Visible = false;
                        exitScheduleButtonsList[i].Visible = false;
                    }

                    for (int i = 0; i < scheduler.Count(); i++)
                    {
                        scheduleButtonsList[i].Visible = true;
                        exitScheduleButtonsList[i].Visible = true;
                    }
                }
            }

            private void scheduleButtonColorUpdate()
            {
                foreach (Button currButton in scheduleButtonsList)
                {
                    if (currButton.Visible)
                    {
                        currButton.BackColor = Color.LightGray;
                    }
                }

                scheduleButtonsList[currentScheduleIndex].BackColor = Color.AliceBlue;
            }

            private void Add_Schedule_Button_Click(object sender, EventArgs e)
            {
                if (scheduler.Count() < 6)
                {
                    scheduleButtonsList[scheduler.Count()].Visible = true;
                    exitScheduleButtonsList[scheduler.Count()].Visible = true;
                    scheduler.Add(new Scheduler());
                    currentScheduleIndex = scheduler.Count - 1;
                    updateAddedCourses();
                    scheduleButtonColorUpdate();
                    clearCalender();
                    calendarUpdate();
                }
            }

            #region Schedule Exit Buttons Click Events
            private void Schedule1_EXIT_Button_Click(object sender, EventArgs e)
            {
                if (Schedule1_EXIT_Button.Visible)
                {
                    if (currentScheduleIndex > 0)
                        currentScheduleIndex -= 1;

                    scheduler.Remove(scheduler[0]);
                    scheduleButtonColorUpdate();
                    resetScheduleButtons();
                    clearCalender();
                    updateAddedCourses();
                    calendarUpdate();
                }
            }

            private void Schedule2_EXIT_Button_Click(object sender, EventArgs e)
            {
                if (Schedule2_EXIT_Button.Visible)
                {
                    if (currentScheduleIndex >= 1)
                        currentScheduleIndex -= 1;

                    scheduler.Remove(scheduler[1]);
                    scheduleButtonColorUpdate();
                    resetScheduleButtons();
                    clearCalender();
                    updateAddedCourses();
                    calendarUpdate();
                }
            }

            private void Schedule3_EXIT_Button_Click(object sender, EventArgs e)
            {
                if (Schedule3_EXIT_Button.Visible)
                {
                    if (currentScheduleIndex >= 2)
                        currentScheduleIndex -= 1;

                    scheduler.Remove(scheduler[2]);
                    scheduleButtonColorUpdate();
                    resetScheduleButtons();
                    clearCalender();
                    updateAddedCourses();
                    calendarUpdate();
                }
            }

            private void Schedule4_EXIT_Button_Click(object sender, EventArgs e)
            {
                if (Schedule4_EXIT_Button.Visible)
                {
                    if (currentScheduleIndex >= 3)
                        currentScheduleIndex -= 1;

                    scheduler.Remove(scheduler[3]);
                    scheduleButtonColorUpdate();
                    resetScheduleButtons();
                    clearCalender();
                    updateAddedCourses();
                    calendarUpdate();
                }
            }

            private void Schedule5_EXIT_Button_Click(object sender, EventArgs e)
            {
                if (Schedule5_EXIT_Button.Visible)
                {
                    if (currentScheduleIndex >= 4)
                        currentScheduleIndex -= 1;

                    scheduler.Remove(scheduler[4]);
                    scheduleButtonColorUpdate();
                    resetScheduleButtons();
                    clearCalender();
                    updateAddedCourses();
                    calendarUpdate();
                }
            }

            private void Schedule6_EXIT_Button_Click(object sender, EventArgs e)
            {
                if (Schedule6_EXIT_Button.Visible)
                {
                    if (currentScheduleIndex >= 5)
                        currentScheduleIndex -= 1;

                    scheduler.Remove(scheduler[5]);
                    scheduleButtonColorUpdate();
                    resetScheduleButtons();
                    clearCalender();
                    updateAddedCourses();
                    calendarUpdate();
                }
            }
            #endregion

            #region Schedule Button Click Events
            private void Schedule1_Button_Click(object sender, EventArgs e)
            {
                if (Schedule1_Button.Visible)
                {
                    currentScheduleIndex = 0;
                    clearCalender();
                    updateAddedCourses();
                    scheduleButtonColorUpdate();
                    calendarUpdate();
                }

            }

            private void Schedule2_button_Click(object sender, EventArgs e)
            {
                if (Schedule2_Button.Visible)
                {
                    currentScheduleIndex = 1;
                    clearCalender();
                    updateAddedCourses();
                    scheduleButtonColorUpdate();
                    calendarUpdate();
                }
            }

            private void Schedule3_Button_Click(object sender, EventArgs e)
            {
                if (Schedule3_Button.Visible)
                {
                    currentScheduleIndex = 2;
                    clearCalender();
                    updateAddedCourses();
                    scheduleButtonColorUpdate();
                    calendarUpdate();
                }
            }

            private void Schedule4_Button_Click(object sender, EventArgs e)
            {
                if (Schedule4_Button.Visible)
                {
                    currentScheduleIndex = 3;
                    clearCalender();
                    updateAddedCourses();
                    scheduleButtonColorUpdate();
                    calendarUpdate();
                }
            }

            private void Schedule5_Button_Click(object sender, EventArgs e)
            {
                if (Schedule5_Button.Visible)
                {
                    currentScheduleIndex = 4;
                    clearCalender();
                    updateAddedCourses();
                    scheduleButtonColorUpdate();
                    calendarUpdate();
                }
            }

            private void Schedule6_Button_Click(object sender, EventArgs e)
            {
                if (Schedule6_Button.Visible)
                {
                    currentScheduleIndex = 5;
                    clearCalender();
                    updateAddedCourses();
                    scheduleButtonColorUpdate();
                    calendarUpdate();
                }
            }



        #endregion
        #endregion

        #region OtherClickEvents
        
        private void clearlabel_Click(object sender, EventArgs e)
        {

        }



        private void addedCoursesLabel_Click(object sender, EventArgs e)
        {

        }

        private void timePicker_Click(object sender, EventArgs e)
        {
        }

        private void dayTimeButton_Click(object sender, EventArgs e)
        {
            if (!dayPicker.Visible)
            {
                dayPicker.Visible = true;
                timePicker.Visible = true;
            }
            else
            {
                dayPicker.Visible = false;
                timePicker.Visible = false;
            }
        }

        private void courseCodeBox_Click(object sender, EventArgs e)
        {
            courseCodeBox.Text = String.Empty;
            courseCodeBox.ForeColor = Color.Black;
        }

        private void departmentBox_Click(object sender, EventArgs e)
        {
            departmentBox.Text = String.Empty;
            departmentBox.ForeColor = Color.Black;
        }

        private void searchBox_Click(object sender, EventArgs e)
        {
            searchBox.Text = String.Empty;
            searchBox.ForeColor = Color.Black;
        }

        private void finalsButtonBig_Click(object sender, EventArgs e)
        {
            if (!finalsButtonSmall.Checked)
            {
                finalsButtonSmall.Checked = true;
                check.Visible = true;
                changeLabels(true);
                calendarUpdate_Finals();
            }
            else
            {
                finalsButtonSmall.Checked = false;
                check.Visible = false;
                changeLabels(false);
                clearFinals();
                calendarUpdate();
            }
        }


        #endregion

        #region OtherLeaveEvents

        private void departmentBox_Leave(object sender, EventArgs e)
        {
            departmentBox.Text = "Department";
            departmentBox.ForeColor = Color.Silver;
        }

        private void courseCodeBox_Leave(object sender, EventArgs e)
        {
            courseCodeBox.Text = "Course Code";
            courseCodeBox.ForeColor = Color.Silver;
        }

        private void searchBox_Leave(object sender, EventArgs e)
        {
            searchBox.Text = "Course Name";
            searchBox.ForeColor = Color.Silver;
        }

        #endregion

        #region OtherEvents
        private void finalsButtonSmall_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void departmentBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            departmentLabel.Visible = false;
        }

        private void searchBox_Enter(object sender, EventArgs e)
        {
            searchBox.Text = String.Empty;
            searchBox.ForeColor = Color.Black;
        }

        private string convertTimeSpan(TimeSpan ts)
        {
            char[] time = new char[7];

            if (ts == new TimeSpan(12, 0, 0)) // 12pm
            {
                ts.ToString().CopyTo(0, time, 0, 5);
                time[5] = 'p';
                time[6] = 'm';
            }

            else if (ts > new TimeSpan(12, 0, 0)) // afternoon hours
            {
                ts = new TimeSpan(ts.Hours - 12, ts.Minutes, ts.Seconds);

                ts.ToString().CopyTo(1, time, 0, 4);
                time[4] = 'p';
                time[5] = 'm';
            }

            else if (ts < new TimeSpan(10, 0, 0)) // before 10am
            {
                ts.ToString().CopyTo(1, time, 0, 4);
                time[4] = 'a';
                time[5] = 'm';
            }
            else
            {
                ts.ToString().CopyTo(0, time, 0, 5); // 10am - 1pm
                time[5] = 'a';
                time[6] = 'm';
            }

            return new String(time);
        }

        private void dayTimeButton_Leave(object sender, EventArgs e)
        {
            dayPicker.Visible = false;
            timePicker.Visible = false;
        }

        private void timePicker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void courseCodeBox_Enter(object sender, EventArgs e)
        {
            courseCodeBox.Text = String.Empty;
            courseCodeBox.ForeColor = Color.Black;
            courseCodeLabel.Visible = false;
        }

        private void departmentBox_Enter(object sender, EventArgs e)
        {
            departmentBox.Text = String.Empty;
            departmentBox.ForeColor = Color.Black;
            departmentLabel.Visible = false;
        }

        #endregion

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void added_courses_panel_MouseHover(object sender, EventArgs e)
        {
            removetooltip.Visible = true;
        }

        private void added_courses_panel_MouseLeave(object sender, EventArgs e)
        {
            removetooltip.Visible = false;
        }
    }
}

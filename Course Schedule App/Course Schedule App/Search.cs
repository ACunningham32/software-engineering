﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Schedule_App
{
    class Search
    {
        //The basic search functions.

        /// <summary>
        /// Searches the course list by time
        /// </summary>
        /// <param name="start"></param>
        /// <param name="searchList"></param>
        /// <returns></returns>
        static List<course> courseSearchTime(string start, ref List<course> searchList)
        {
            List<course> rv = new List<course>();
            if (start == "")
                return searchList;
            foreach (course current in searchList)
                if (current.BeginString == start)
                    rv.Add(current);
            return rv;
        }


        /// <summary>
        /// Searches the course list based on a list of days.
        /// </summary>
        /// <param name="checkedItems"></param>
        /// <param name="searchList"></param>
        /// <returns></returns>
        static List<course> courseSearchDay(bool[] checkedItems, ref List<course> searchList)
        {
            List<course> rv = new List<course>();
            string[] days = {"M", "T", "W", "R", "F" };
            string searchTerm = "";
            for (int i = 0; i < days.Length; i++)
                if (checkedItems[i] == true)
                    searchTerm += days[i];

            if (searchTerm == "")
                return searchList;

            foreach (course current in searchList)
                foreach(char c in searchTerm)
                {
                    if (current.Day.Contains(c) || current.Day == "NULL")
                        rv.Add(current);
                }
            return rv;
        }

        /// <summary>
        /// Searches the course list by department.
        /// </summary>
        /// <param name="term"></param>
        /// <param name="searchList"></param>
        /// <returns></returns>
        static List<course> courseSearchDepartment(string term, ref List<course> searchList)
        {
            List<course> rv = new List<course>();
            if (term == "")
                return searchList;
            foreach (course current in searchList)
                if (current.Department == term)
                    rv.Add(current);            
            return rv;
        }

        /// <summary>
        /// Searches the course list by course number.
        /// </summary>
        /// <param name="term"></param>
        /// <param name="searchList"></param>
        /// <returns></returns>
        static List<course> courseSearchNumber(string term, ref List<course> searchList)
        {
            List<course> rv = new List<course>();
            if (term == "")
                return searchList;
            foreach (course current in searchList)
                if(current.CourseNumber == term)
                    rv.Add(current);
            return rv;
        }

        /// <summary>
        /// Searches the course list by title.
        /// </summary>
        /// <param name="term"></param>
        /// <param name="searchList"></param>
        /// <returns></returns>
        static List<course> courseSearchTitle(string term, ref List<course> searchList)
        {
            term = term.ToUpper();
            List<course> rv = new List<course>();
            if (term == "")
                return searchList;
            foreach (course current in searchList)
                if(current.ShortTitle.Contains(term) || current.LongTitle.Contains(term))
                    rv.Add(current);
            
            return rv;
        }
        ////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Checks if the course has room.
        /// </summary>
        /// <param name="searchList"></param>
        /// <returns></returns>
        static List<course> hasRoom(ref List<course> searchList)
        {
            List<course> availables = new List<course>();
            foreach(course current in searchList)
            {
                if (current.Enrolled < current.Capacity)
                    availables.Add(current);
            }
            return availables;
        }

        /// <summary>
        /// Main search function call. Searches all paramaters, and unions the resulting lists.
        /// </summary>
        /// <param name="tstart"></param>
        /// <param name="days"></param>
        /// <param name="courseNumber"></param>
        /// <param name="title"></param>
        /// <param name="department"></param>
        /// <param name="searchList"></param>
        /// <returns></returns>
        static public List<course> fullSearch(string tstart,  bool[] days, string courseNumber, string title, string department, ref List<course> searchList)
        {
            List<List<course>> intersectList = new List<List<course>>();
            List<course> totalResults = new List<course>();
            List<course> dayResults = courseSearchDay(days, ref searchList);
            List<course> timeResults = courseSearchTime(tstart, ref searchList);
            List<course> departmentResults = courseSearchDepartment(department, ref searchList);
            List<course> courseNumberResults = courseSearchNumber(courseNumber, ref searchList);
            List<course> titleResults = courseSearchTitle(title, ref searchList);
            
            intersectList.Add(dayResults);
            intersectList.Add(timeResults);
            intersectList.Add(departmentResults);
            intersectList.Add(courseNumberResults);
            intersectList.Add(titleResults);

            totalResults = iterativeIntersection(intersectList);
            return totalResults;
        }
        
        /// <summary>
        /// An iterative intersection function for returning unique results.
        /// </summary>
        /// <param name="toIntersect"></param>
        /// <returns></returns>
        static List<course> iterativeIntersection(List<List<course>> toIntersect)
        {
            IEnumerable<course> workingSet = toIntersect[0];
            foreach(List<course> a in toIntersect)
            {
                workingSet = workingSet.Intersect(a);
            }
            return workingSet.ToList<course>();
        }

    }
}

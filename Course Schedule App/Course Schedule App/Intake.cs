﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Schedule_App
{
    class Intake
    {
        /// <summary>
        /// This function converts a CSV file into a 2d-List of strings. Likely innefficient, but provides exposure of the functions output.
        /// Recommend that it return, rather, a list of course.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static List<List<string>> parseCourseFile(string path)
        {
            List<List<string>> parsedCourses = new List<List<string>>();
            string[] courseBody = File.ReadAllLines(path);
            foreach (string line in courseBody)
            {
                List<string> courseInfo = new List<string>();
                string[] split = line.Split(',');
                foreach (string data in split)
                {
                    courseInfo.Add(data);
                }
                parsedCourses.Add(courseInfo);
            }
            parsedCourses.RemoveAt(0);
            return parsedCourses;
        }

        /// <summary>
        /// This function converts a 2d List of strings into a List of courses
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<course> makeCourseList(string path)
        {
            List<List<string>> data = parseCourseFile(path);
            List<course> courses = new List<course>();
            foreach (List<string> line in data)
            {
                course temp = new course(line);
                courses.Add(temp);
            }
            return courses;
        }
        
    }
}

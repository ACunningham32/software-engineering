﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Schedule_App
{
    class Scheduler
    {

        public List<course> allCourses = new List<course>();
        public List<course> scheduledCourses = new List<course>();

        string path = "courses.csv";

        //Constructor: creates Courses Obj. and stores course data within each
        //The Courses objs created are then stored within the allCourses List of Courses objs
        public Scheduler()
        {
            allCourses = Intake.makeCourseList(path);
        }

        //Removes the specified Courses obj. from the sceduledCourses list
        public void RemoveCourse(course removedCourse)
        {
            foreach (course currCourse in scheduledCourses)
                if (currCourse.LongTitle == removedCourse.LongTitle && currCourse.ShortTitle == removedCourse.ShortTitle)
                {
                    scheduledCourses.RemoveAt(scheduledCourses.IndexOf(currCourse));
                }
        }
    }
}

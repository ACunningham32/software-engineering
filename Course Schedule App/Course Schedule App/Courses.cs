﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Schedule_App
{
    class Courses
    {
        private string _code;
        private char _section;
        private string _shortTitle;
        private string _longTitle;
        private string _beginTime;
        private string _endTime;
        private string _meets;
        private string _building;
        private int _roomNumber;
        private int _enrollment;
        private int _capacity;

        //constructor: sets time, name, prof, credits, day, number, and section for a new Courses object
        public Courses(string course_code, char course_section, string course_shortTitle, string course_longTitle, string course_beginTime, 
            string course_endTime, string course_meets, string course_building, int course_roomNumber, int course_enrollment, int course_capacity)
        {
            _code = course_code;
            _section = course_section;
            _shortTitle = course_shortTitle;
            _longTitle = course_longTitle;
            _beginTime = course_beginTime;
            _endTime = course_endTime;
            _meets = course_meets;
            _building = course_building;
            _roomNumber = course_roomNumber;
            _enrollment = course_enrollment;
            _capacity = course_capacity;
        }

        //returns course's section (string)
        public char section
        {
            get
            {
                return _section;
            }
        }

        //returns course's code (string)
        public string code
        {
            get
            {
                return _code;
            }
        }

        //returns course's short title (string)
        public string shortTitle
        {
            get
            {
                return _shortTitle;
            }
        }

        //returns course's long title (string)
        public string longTitle
        {
            get
            {
                return _longTitle;
            }
        }

        //returns course's start time (string)
        public string beginTime
        {
            get
            {
                return _beginTime;
            }
        }

        //returns course's end time (string)
        public string endTime
        {
            get
            {
                return _endTime;
            }
        }

        //returns  course's meeting days (string)
        public string meets
        {
            get
            {
                return _meets;
            }
        }

        //returns course's building (string)
        public string building
        {
            get
            {
                return _building;
            }
        }

        //returns course's roomNumber (int)
        public int roomNumber
        {
            get
            {
                return _roomNumber;
            }
        }

        //returns course's enrollment number (int)
        public int enrollment
        {
            get
            {
                return _enrollment;
            }
        }

        ///returns course's total capacity number (int)
        public int capacity
        {
            get
            {
                return _capacity;
            }
        }
    }
}

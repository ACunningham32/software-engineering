﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Course_Schedule_App
{
    class Result    :   Button
    {
        private int elementNumber = 0;
        public int ElementNumber
        {
            get { return elementNumber; }
            set { elementNumber = value; }
        }
    }
}

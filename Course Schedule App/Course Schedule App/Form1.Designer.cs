﻿namespace Course_Schedule_App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.departmentBox = new System.Windows.Forms.ComboBox();
            this.courseCodeBox = new System.Windows.Forms.ComboBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.finalsButtonBig = new System.Windows.Forms.Button();
            this.mondayLabel = new System.Windows.Forms.Label();
            this.tuesdayLabel = new System.Windows.Forms.Label();
            this.wednesdayLabel = new System.Windows.Forms.Label();
            this.thursdayLabel = new System.Windows.Forms.Label();
            this.fridayLabel = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.addedCoursesLabel = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.M9A = new System.Windows.Forms.Button();
            this.M11A = new System.Windows.Forms.Button();
            this.M10A = new System.Windows.Forms.Button();
            this.M3P = new System.Windows.Forms.Button();
            this.M2P = new System.Windows.Forms.Button();
            this.M1P = new System.Windows.Forms.Button();
            this.M12P = new System.Windows.Forms.Button();
            this.M6P = new System.Windows.Forms.Button();
            this.M5P = new System.Windows.Forms.Button();
            this.M4P = new System.Windows.Forms.Button();
            this.T6P = new System.Windows.Forms.Button();
            this.T4P = new System.Windows.Forms.Button();
            this.T2P = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.T1P = new System.Windows.Forms.Button();
            this.T11A = new System.Windows.Forms.Button();
            this.T10A = new System.Windows.Forms.Button();
            this.T8A = new System.Windows.Forms.Button();
            this.W5P = new System.Windows.Forms.Button();
            this.W4P = new System.Windows.Forms.Button();
            this.W3P = new System.Windows.Forms.Button();
            this.W2P = new System.Windows.Forms.Button();
            this.W1P = new System.Windows.Forms.Button();
            this.W12P = new System.Windows.Forms.Button();
            this.W11A = new System.Windows.Forms.Button();
            this.W10A = new System.Windows.Forms.Button();
            this.W9A = new System.Windows.Forms.Button();
            this.W8A = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.R6P = new System.Windows.Forms.Button();
            this.R4P = new System.Windows.Forms.Button();
            this.R2P = new System.Windows.Forms.Button();
            this.R1P = new System.Windows.Forms.Button();
            this.R11A = new System.Windows.Forms.Button();
            this.R10A = new System.Windows.Forms.Button();
            this.R8A = new System.Windows.Forms.Button();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.F6P = new System.Windows.Forms.Button();
            this.F5P = new System.Windows.Forms.Button();
            this.F4P = new System.Windows.Forms.Button();
            this.F3P = new System.Windows.Forms.Button();
            this.F2P = new System.Windows.Forms.Button();
            this.F1P = new System.Windows.Forms.Button();
            this.F12P = new System.Windows.Forms.Button();
            this.F11A = new System.Windows.Forms.Button();
            this.F10A = new System.Windows.Forms.Button();
            this.F9A = new System.Windows.Forms.Button();
            this.F8A = new System.Windows.Forms.Button();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.W6P = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.dayPicker = new System.Windows.Forms.CheckedListBox();
            this.timePicker = new System.Windows.Forms.ComboBox();
            this.dayTimeButton = new System.Windows.Forms.Button();
            this.M8A = new System.Windows.Forms.Button();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.courseCodeLabel = new System.Windows.Forms.Label();
            this.finalsButtonSmall = new System.Windows.Forms.CheckBox();
            this.check = new System.Windows.Forms.Button();
            this.FR7 = new System.Windows.Forms.Button();
            this.FF7 = new System.Windows.Forms.Button();
            this.FF2 = new System.Windows.Forms.Button();
            this.FF9 = new System.Windows.Forms.Button();
            this.FS7 = new System.Windows.Forms.Button();
            this.FS2 = new System.Windows.Forms.Button();
            this.FS9 = new System.Windows.Forms.Button();
            this.FM7 = new System.Windows.Forms.Button();
            this.FM2 = new System.Windows.Forms.Button();
            this.FM9 = new System.Windows.Forms.Button();
            this.FT7 = new System.Windows.Forms.Button();
            this.FT2 = new System.Windows.Forms.Button();
            this.FT9 = new System.Windows.Forms.Button();
            this.search_results_panel = new System.Windows.Forms.FlowLayoutPanel();
            this.added_courses_panel = new System.Windows.Forms.FlowLayoutPanel();
            this.Add_Schedule_Button = new System.Windows.Forms.Button();
            this.Schedule1_Button = new System.Windows.Forms.Button();
            this.Schedule2_Button = new System.Windows.Forms.Button();
            this.Schedule3_Button = new System.Windows.Forms.Button();
            this.Schedule4_Button = new System.Windows.Forms.Button();
            this.Schedule5_Button = new System.Windows.Forms.Button();
            this.Schedule6_Button = new System.Windows.Forms.Button();
            this.Schedule1_EXIT_Button = new System.Windows.Forms.Button();
            this.Schedule2_EXIT_Button = new System.Windows.Forms.Button();
            this.Schedule3_EXIT_Button = new System.Windows.Forms.Button();
            this.Schedule4_EXIT_Button = new System.Windows.Forms.Button();
            this.Schedule5_EXIT_Button = new System.Windows.Forms.Button();
            this.Schedule6_EXIT_Button = new System.Windows.Forms.Button();
            this.removetooltip = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(-1, -1);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(1094, 65);
            this.textBox1.TabIndex = 0;
            // 
            // departmentBox
            // 
            this.departmentBox.BackColor = System.Drawing.Color.White;
            this.departmentBox.DropDownHeight = 500;
            this.departmentBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.departmentBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.departmentBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentBox.ForeColor = System.Drawing.Color.White;
            this.departmentBox.FormattingEnabled = true;
            this.departmentBox.IntegralHeight = false;
            this.departmentBox.Location = new System.Drawing.Point(198, 16);
            this.departmentBox.Margin = new System.Windows.Forms.Padding(4);
            this.departmentBox.MaxDropDownItems = 20;
            this.departmentBox.Name = "departmentBox";
            this.departmentBox.Size = new System.Drawing.Size(172, 33);
            this.departmentBox.TabIndex = 2;
            this.departmentBox.Tag = "";
            this.departmentBox.SelectedIndexChanged += new System.EventHandler(this.departmentBox_SelectedIndexChanged);
            this.departmentBox.Click += new System.EventHandler(this.departmentBox_Click);
            this.departmentBox.Enter += new System.EventHandler(this.departmentBox_Enter);
            // 
            // courseCodeBox
            // 
            this.courseCodeBox.BackColor = System.Drawing.Color.White;
            this.courseCodeBox.DropDownHeight = 500;
            this.courseCodeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.courseCodeBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.courseCodeBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.courseCodeBox.ForeColor = System.Drawing.Color.White;
            this.courseCodeBox.FormattingEnabled = true;
            this.courseCodeBox.IntegralHeight = false;
            this.courseCodeBox.Location = new System.Drawing.Point(378, 15);
            this.courseCodeBox.Margin = new System.Windows.Forms.Padding(4);
            this.courseCodeBox.MaxDropDownItems = 20;
            this.courseCodeBox.Name = "courseCodeBox";
            this.courseCodeBox.Size = new System.Drawing.Size(172, 33);
            this.courseCodeBox.TabIndex = 3;
            this.courseCodeBox.Click += new System.EventHandler(this.courseCodeBox_Click);
            this.courseCodeBox.Enter += new System.EventHandler(this.courseCodeBox_Enter);
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.AliceBlue;
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(12, 98);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(796, 572);
            this.textBox4.TabIndex = 11;
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(60, 107);
            this.textBox10.Margin = new System.Windows.Forms.Padding(4);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(140, 35);
            this.textBox10.TabIndex = 18;
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(206, 107);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(140, 35);
            this.textBox6.TabIndex = 19;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(354, 105);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(140, 35);
            this.textBox7.TabIndex = 20;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(498, 107);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(140, 35);
            this.textBox8.TabIndex = 21;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(648, 105);
            this.textBox9.Margin = new System.Windows.Forms.Padding(4);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(140, 35);
            this.textBox9.TabIndex = 22;
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 144);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "8:00";
            // 
            // textBox11
            // 
            this.textBox11.Enabled = false;
            this.textBox11.Location = new System.Drawing.Point(60, 146);
            this.textBox11.Margin = new System.Windows.Forms.Padding(4);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(140, 517);
            this.textBox11.TabIndex = 66;
            // 
            // textBox12
            // 
            this.textBox12.Enabled = false;
            this.textBox12.Location = new System.Drawing.Point(206, 146);
            this.textBox12.Margin = new System.Windows.Forms.Padding(4);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(140, 517);
            this.textBox12.TabIndex = 67;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 183);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 71;
            this.label2.Text = "9:00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 610);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 72;
            this.label3.Text = "8:00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 583);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 74;
            this.label5.Text = "7:00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 543);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 75;
            this.label6.Text = "6:00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.AliceBlue;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 503);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 76;
            this.label7.Text = "5:00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 463);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 77;
            this.label8.Text = "4:00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.AliceBlue;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 422);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 78;
            this.label9.Text = "3:00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.AliceBlue;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(24, 383);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 79;
            this.label10.Text = "2:00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.AliceBlue;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(24, 343);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 80;
            this.label11.Text = "1:00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.AliceBlue;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(18, 303);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 81;
            this.label12.Text = "12:00";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.AliceBlue;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(18, 263);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 82;
            this.label13.Text = "11:00";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.AliceBlue;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(18, 223);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 83;
            this.label14.Text = "10:00";
            // 
            // finalsButtonBig
            // 
            this.finalsButtonBig.BackColor = System.Drawing.Color.AliceBlue;
            this.finalsButtonBig.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finalsButtonBig.Location = new System.Drawing.Point(12, 9);
            this.finalsButtonBig.Margin = new System.Windows.Forms.Padding(4);
            this.finalsButtonBig.Name = "finalsButtonBig";
            this.finalsButtonBig.Size = new System.Drawing.Size(80, 49);
            this.finalsButtonBig.TabIndex = 85;
            this.finalsButtonBig.Text = "Finals";
            this.finalsButtonBig.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.finalsButtonBig.UseVisualStyleBackColor = false;
            this.finalsButtonBig.Click += new System.EventHandler(this.finalsButtonBig_Click);
            // 
            // mondayLabel
            // 
            this.mondayLabel.AutoSize = true;
            this.mondayLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mondayLabel.Location = new System.Drawing.Point(88, 110);
            this.mondayLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mondayLabel.Name = "mondayLabel";
            this.mondayLabel.Size = new System.Drawing.Size(86, 25);
            this.mondayLabel.TabIndex = 87;
            this.mondayLabel.Text = "Monday";
            this.mondayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tuesdayLabel
            // 
            this.tuesdayLabel.AutoSize = true;
            this.tuesdayLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tuesdayLabel.Location = new System.Drawing.Point(234, 110);
            this.tuesdayLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tuesdayLabel.Name = "tuesdayLabel";
            this.tuesdayLabel.Size = new System.Drawing.Size(84, 25);
            this.tuesdayLabel.TabIndex = 88;
            this.tuesdayLabel.Text = "Tuesday";
            this.tuesdayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // wednesdayLabel
            // 
            this.wednesdayLabel.AutoSize = true;
            this.wednesdayLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wednesdayLabel.Location = new System.Drawing.Point(364, 110);
            this.wednesdayLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wednesdayLabel.Name = "wednesdayLabel";
            this.wednesdayLabel.Size = new System.Drawing.Size(114, 25);
            this.wednesdayLabel.TabIndex = 89;
            this.wednesdayLabel.Text = "Wednesday";
            this.wednesdayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // thursdayLabel
            // 
            this.thursdayLabel.AutoSize = true;
            this.thursdayLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thursdayLabel.Location = new System.Drawing.Point(524, 110);
            this.thursdayLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.thursdayLabel.Name = "thursdayLabel";
            this.thursdayLabel.Size = new System.Drawing.Size(94, 25);
            this.thursdayLabel.TabIndex = 90;
            this.thursdayLabel.Text = "Thursday";
            this.thursdayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fridayLabel
            // 
            this.fridayLabel.AutoSize = true;
            this.fridayLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fridayLabel.Location = new System.Drawing.Point(686, 110);
            this.fridayLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fridayLabel.Name = "fridayLabel";
            this.fridayLabel.Size = new System.Drawing.Size(67, 25);
            this.fridayLabel.TabIndex = 91;
            this.fridayLabel.Text = "Friday";
            this.fridayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.SystemColors.Control;
            this.textBox16.Enabled = false;
            this.textBox16.Location = new System.Drawing.Point(816, 352);
            this.textBox16.Margin = new System.Windows.Forms.Padding(4);
            this.textBox16.Multiline = true;
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(268, 44);
            this.textBox16.TabIndex = 100;
            // 
            // addedCoursesLabel
            // 
            this.addedCoursesLabel.AutoSize = true;
            this.addedCoursesLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addedCoursesLabel.Location = new System.Drawing.Point(883, 355);
            this.addedCoursesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.addedCoursesLabel.Name = "addedCoursesLabel";
            this.addedCoursesLabel.Size = new System.Drawing.Size(139, 25);
            this.addedCoursesLabel.TabIndex = 101;
            this.addedCoursesLabel.Text = "Added Courses";
            this.addedCoursesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.addedCoursesLabel.Click += new System.EventHandler(this.addedCoursesLabel_Click);
            // 
            // searchBox
            // 
            this.searchBox.BackColor = System.Drawing.Color.White;
            this.searchBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBox.ForeColor = System.Drawing.Color.Silver;
            this.searchBox.Location = new System.Drawing.Point(734, 15);
            this.searchBox.Margin = new System.Windows.Forms.Padding(4);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(264, 33);
            this.searchBox.TabIndex = 109;
            this.searchBox.Text = "Course Name";
            this.searchBox.Click += new System.EventHandler(this.searchBox_Click);
            this.searchBox.Enter += new System.EventHandler(this.searchBox_Enter);
            // 
            // M9A
            // 
            this.M9A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M9A.Enabled = false;
            this.M9A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M9A.FlatAppearance.BorderSize = 10;
            this.M9A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M9A.Location = new System.Drawing.Point(60, 187);
            this.M9A.Margin = new System.Windows.Forms.Padding(4);
            this.M9A.Name = "M9A";
            this.M9A.Size = new System.Drawing.Size(138, 35);
            this.M9A.TabIndex = 111;
            this.M9A.UseVisualStyleBackColor = false;
            this.M9A.Visible = false;
            // 
            // M11A
            // 
            this.M11A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M11A.Enabled = false;
            this.M11A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M11A.FlatAppearance.BorderSize = 10;
            this.M11A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M11A.Location = new System.Drawing.Point(60, 266);
            this.M11A.Margin = new System.Windows.Forms.Padding(4);
            this.M11A.Name = "M11A";
            this.M11A.Size = new System.Drawing.Size(138, 35);
            this.M11A.TabIndex = 113;
            this.M11A.UseVisualStyleBackColor = false;
            this.M11A.Visible = false;
            // 
            // M10A
            // 
            this.M10A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M10A.Enabled = false;
            this.M10A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M10A.FlatAppearance.BorderSize = 10;
            this.M10A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M10A.Location = new System.Drawing.Point(60, 227);
            this.M10A.Margin = new System.Windows.Forms.Padding(4);
            this.M10A.Name = "M10A";
            this.M10A.Size = new System.Drawing.Size(138, 35);
            this.M10A.TabIndex = 112;
            this.M10A.UseVisualStyleBackColor = false;
            this.M10A.Visible = false;
            // 
            // M3P
            // 
            this.M3P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M3P.Enabled = false;
            this.M3P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M3P.FlatAppearance.BorderSize = 10;
            this.M3P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M3P.Location = new System.Drawing.Point(60, 427);
            this.M3P.Margin = new System.Windows.Forms.Padding(4);
            this.M3P.Name = "M3P";
            this.M3P.Size = new System.Drawing.Size(138, 35);
            this.M3P.TabIndex = 117;
            this.M3P.UseVisualStyleBackColor = false;
            this.M3P.Visible = false;
            // 
            // M2P
            // 
            this.M2P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M2P.Enabled = false;
            this.M2P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M2P.FlatAppearance.BorderSize = 10;
            this.M2P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M2P.Location = new System.Drawing.Point(60, 387);
            this.M2P.Margin = new System.Windows.Forms.Padding(4);
            this.M2P.Name = "M2P";
            this.M2P.Size = new System.Drawing.Size(138, 35);
            this.M2P.TabIndex = 116;
            this.M2P.UseVisualStyleBackColor = false;
            this.M2P.Visible = false;
            // 
            // M1P
            // 
            this.M1P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M1P.Enabled = false;
            this.M1P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M1P.FlatAppearance.BorderSize = 10;
            this.M1P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M1P.Location = new System.Drawing.Point(60, 347);
            this.M1P.Margin = new System.Windows.Forms.Padding(4);
            this.M1P.Name = "M1P";
            this.M1P.Size = new System.Drawing.Size(138, 35);
            this.M1P.TabIndex = 115;
            this.M1P.UseVisualStyleBackColor = false;
            this.M1P.Visible = false;
            // 
            // M12P
            // 
            this.M12P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M12P.Enabled = false;
            this.M12P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M12P.FlatAppearance.BorderSize = 10;
            this.M12P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M12P.Location = new System.Drawing.Point(60, 307);
            this.M12P.Margin = new System.Windows.Forms.Padding(4);
            this.M12P.Name = "M12P";
            this.M12P.Size = new System.Drawing.Size(138, 35);
            this.M12P.TabIndex = 114;
            this.M12P.UseVisualStyleBackColor = false;
            this.M12P.Visible = false;
            // 
            // M6P
            // 
            this.M6P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M6P.Enabled = false;
            this.M6P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M6P.FlatAppearance.BorderSize = 10;
            this.M6P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M6P.Location = new System.Drawing.Point(60, 561);
            this.M6P.Margin = new System.Windows.Forms.Padding(4);
            this.M6P.Name = "M6P";
            this.M6P.Size = new System.Drawing.Size(138, 100);
            this.M6P.TabIndex = 120;
            this.M6P.UseVisualStyleBackColor = false;
            this.M6P.Visible = false;
            // 
            // M5P
            // 
            this.M5P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M5P.Enabled = false;
            this.M5P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M5P.FlatAppearance.BorderSize = 10;
            this.M5P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M5P.Location = new System.Drawing.Point(60, 508);
            this.M5P.Margin = new System.Windows.Forms.Padding(4);
            this.M5P.Name = "M5P";
            this.M5P.Size = new System.Drawing.Size(138, 35);
            this.M5P.TabIndex = 119;
            this.M5P.UseVisualStyleBackColor = false;
            this.M5P.Visible = false;
            // 
            // M4P
            // 
            this.M4P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.M4P.Enabled = false;
            this.M4P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M4P.FlatAppearance.BorderSize = 10;
            this.M4P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M4P.Location = new System.Drawing.Point(60, 467);
            this.M4P.Margin = new System.Windows.Forms.Padding(4);
            this.M4P.Name = "M4P";
            this.M4P.Size = new System.Drawing.Size(138, 35);
            this.M4P.TabIndex = 118;
            this.M4P.UseVisualStyleBackColor = false;
            this.M4P.Visible = false;
            // 
            // T6P
            // 
            this.T6P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.T6P.Enabled = false;
            this.T6P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.T6P.FlatAppearance.BorderSize = 10;
            this.T6P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T6P.Location = new System.Drawing.Point(208, 561);
            this.T6P.Margin = new System.Windows.Forms.Padding(4);
            this.T6P.Name = "T6P";
            this.T6P.Size = new System.Drawing.Size(138, 100);
            this.T6P.TabIndex = 133;
            this.T6P.UseVisualStyleBackColor = false;
            this.T6P.Visible = false;
            // 
            // T4P
            // 
            this.T4P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.T4P.Enabled = false;
            this.T4P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.T4P.FlatAppearance.BorderSize = 10;
            this.T4P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T4P.Location = new System.Drawing.Point(208, 467);
            this.T4P.Margin = new System.Windows.Forms.Padding(4);
            this.T4P.Name = "T4P";
            this.T4P.Size = new System.Drawing.Size(138, 74);
            this.T4P.TabIndex = 131;
            this.T4P.UseVisualStyleBackColor = false;
            this.T4P.Visible = false;
            // 
            // T2P
            // 
            this.T2P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.T2P.Enabled = false;
            this.T2P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.T2P.FlatAppearance.BorderSize = 10;
            this.T2P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T2P.Location = new System.Drawing.Point(208, 404);
            this.T2P.Margin = new System.Windows.Forms.Padding(4);
            this.T2P.Name = "T2P";
            this.T2P.Size = new System.Drawing.Size(138, 52);
            this.T2P.TabIndex = 130;
            this.T2P.UseVisualStyleBackColor = false;
            this.T2P.Visible = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button20.Enabled = false;
            this.button20.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button20.FlatAppearance.BorderSize = 10;
            this.button20.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Location = new System.Drawing.Point(208, 387);
            this.button20.Margin = new System.Windows.Forms.Padding(4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(0, 0);
            this.button20.TabIndex = 129;
            this.button20.UseVisualStyleBackColor = false;
            // 
            // T1P
            // 
            this.T1P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.T1P.Enabled = false;
            this.T1P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.T1P.FlatAppearance.BorderSize = 10;
            this.T1P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T1P.Location = new System.Drawing.Point(208, 347);
            this.T1P.Margin = new System.Windows.Forms.Padding(4);
            this.T1P.Name = "T1P";
            this.T1P.Size = new System.Drawing.Size(138, 52);
            this.T1P.TabIndex = 128;
            this.T1P.UseVisualStyleBackColor = false;
            this.T1P.Visible = false;
            // 
            // T11A
            // 
            this.T11A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.T11A.Enabled = false;
            this.T11A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.T11A.FlatAppearance.BorderSize = 10;
            this.T11A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T11A.Location = new System.Drawing.Point(208, 290);
            this.T11A.Margin = new System.Windows.Forms.Padding(4);
            this.T11A.Name = "T11A";
            this.T11A.Size = new System.Drawing.Size(138, 52);
            this.T11A.TabIndex = 127;
            this.T11A.UseVisualStyleBackColor = false;
            this.T11A.Visible = false;
            // 
            // T10A
            // 
            this.T10A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.T10A.Enabled = false;
            this.T10A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.T10A.FlatAppearance.BorderSize = 10;
            this.T10A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T10A.Location = new System.Drawing.Point(208, 233);
            this.T10A.Margin = new System.Windows.Forms.Padding(4);
            this.T10A.Name = "T10A";
            this.T10A.Size = new System.Drawing.Size(138, 52);
            this.T10A.TabIndex = 125;
            this.T10A.UseVisualStyleBackColor = false;
            this.T10A.Visible = false;
            // 
            // T8A
            // 
            this.T8A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.T8A.Enabled = false;
            this.T8A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.T8A.FlatAppearance.BorderSize = 10;
            this.T8A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T8A.Location = new System.Drawing.Point(208, 147);
            this.T8A.Margin = new System.Windows.Forms.Padding(4);
            this.T8A.Name = "T8A";
            this.T8A.Size = new System.Drawing.Size(138, 52);
            this.T8A.TabIndex = 123;
            this.T8A.UseVisualStyleBackColor = false;
            this.T8A.Visible = false;
            // 
            // W5P
            // 
            this.W5P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W5P.Enabled = false;
            this.W5P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W5P.FlatAppearance.BorderSize = 10;
            this.W5P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W5P.Location = new System.Drawing.Point(354, 508);
            this.W5P.Margin = new System.Windows.Forms.Padding(4);
            this.W5P.Name = "W5P";
            this.W5P.Size = new System.Drawing.Size(138, 35);
            this.W5P.TabIndex = 146;
            this.W5P.UseVisualStyleBackColor = false;
            this.W5P.Visible = false;
            // 
            // W4P
            // 
            this.W4P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W4P.Enabled = false;
            this.W4P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W4P.FlatAppearance.BorderSize = 10;
            this.W4P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W4P.Location = new System.Drawing.Point(354, 467);
            this.W4P.Margin = new System.Windows.Forms.Padding(4);
            this.W4P.Name = "W4P";
            this.W4P.Size = new System.Drawing.Size(138, 35);
            this.W4P.TabIndex = 145;
            this.W4P.UseVisualStyleBackColor = false;
            this.W4P.Visible = false;
            // 
            // W3P
            // 
            this.W3P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W3P.Enabled = false;
            this.W3P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W3P.FlatAppearance.BorderSize = 10;
            this.W3P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W3P.Location = new System.Drawing.Point(354, 427);
            this.W3P.Margin = new System.Windows.Forms.Padding(4);
            this.W3P.Name = "W3P";
            this.W3P.Size = new System.Drawing.Size(138, 35);
            this.W3P.TabIndex = 144;
            this.W3P.UseVisualStyleBackColor = false;
            this.W3P.Visible = false;
            // 
            // W2P
            // 
            this.W2P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W2P.Enabled = false;
            this.W2P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W2P.FlatAppearance.BorderSize = 10;
            this.W2P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W2P.Location = new System.Drawing.Point(354, 387);
            this.W2P.Margin = new System.Windows.Forms.Padding(4);
            this.W2P.Name = "W2P";
            this.W2P.Size = new System.Drawing.Size(138, 35);
            this.W2P.TabIndex = 143;
            this.W2P.UseVisualStyleBackColor = false;
            this.W2P.Visible = false;
            // 
            // W1P
            // 
            this.W1P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W1P.Enabled = false;
            this.W1P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W1P.FlatAppearance.BorderSize = 10;
            this.W1P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W1P.Location = new System.Drawing.Point(354, 347);
            this.W1P.Margin = new System.Windows.Forms.Padding(4);
            this.W1P.Name = "W1P";
            this.W1P.Size = new System.Drawing.Size(138, 35);
            this.W1P.TabIndex = 142;
            this.W1P.UseVisualStyleBackColor = false;
            this.W1P.Visible = false;
            // 
            // W12P
            // 
            this.W12P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W12P.Enabled = false;
            this.W12P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W12P.FlatAppearance.BorderSize = 10;
            this.W12P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W12P.Location = new System.Drawing.Point(354, 307);
            this.W12P.Margin = new System.Windows.Forms.Padding(4);
            this.W12P.Name = "W12P";
            this.W12P.Size = new System.Drawing.Size(138, 35);
            this.W12P.TabIndex = 141;
            this.W12P.UseVisualStyleBackColor = false;
            this.W12P.Visible = false;
            // 
            // W11A
            // 
            this.W11A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W11A.Enabled = false;
            this.W11A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W11A.FlatAppearance.BorderSize = 10;
            this.W11A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W11A.Location = new System.Drawing.Point(354, 266);
            this.W11A.Margin = new System.Windows.Forms.Padding(4);
            this.W11A.Name = "W11A";
            this.W11A.Size = new System.Drawing.Size(138, 35);
            this.W11A.TabIndex = 140;
            this.W11A.UseVisualStyleBackColor = false;
            this.W11A.Visible = false;
            // 
            // W10A
            // 
            this.W10A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W10A.Enabled = false;
            this.W10A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W10A.FlatAppearance.BorderSize = 10;
            this.W10A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W10A.Location = new System.Drawing.Point(354, 227);
            this.W10A.Margin = new System.Windows.Forms.Padding(4);
            this.W10A.Name = "W10A";
            this.W10A.Size = new System.Drawing.Size(138, 35);
            this.W10A.TabIndex = 139;
            this.W10A.UseVisualStyleBackColor = false;
            this.W10A.Visible = false;
            // 
            // W9A
            // 
            this.W9A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W9A.Enabled = false;
            this.W9A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W9A.FlatAppearance.BorderSize = 10;
            this.W9A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W9A.Location = new System.Drawing.Point(354, 187);
            this.W9A.Margin = new System.Windows.Forms.Padding(4);
            this.W9A.Name = "W9A";
            this.W9A.Size = new System.Drawing.Size(138, 35);
            this.W9A.TabIndex = 138;
            this.W9A.UseVisualStyleBackColor = false;
            this.W9A.Visible = false;
            // 
            // W8A
            // 
            this.W8A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W8A.Enabled = false;
            this.W8A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W8A.FlatAppearance.BorderSize = 10;
            this.W8A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W8A.Location = new System.Drawing.Point(354, 147);
            this.W8A.Margin = new System.Windows.Forms.Padding(4);
            this.W8A.Name = "W8A";
            this.W8A.Size = new System.Drawing.Size(138, 35);
            this.W8A.TabIndex = 137;
            this.W8A.UseVisualStyleBackColor = false;
            this.W8A.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(352, 146);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(140, 517);
            this.textBox2.TabIndex = 136;
            // 
            // R6P
            // 
            this.R6P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.R6P.Enabled = false;
            this.R6P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.R6P.FlatAppearance.BorderSize = 10;
            this.R6P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R6P.Location = new System.Drawing.Point(500, 561);
            this.R6P.Margin = new System.Windows.Forms.Padding(4);
            this.R6P.Name = "R6P";
            this.R6P.Size = new System.Drawing.Size(138, 100);
            this.R6P.TabIndex = 161;
            this.R6P.UseVisualStyleBackColor = false;
            this.R6P.Visible = false;
            // 
            // R4P
            // 
            this.R4P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.R4P.Enabled = false;
            this.R4P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.R4P.FlatAppearance.BorderSize = 10;
            this.R4P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R4P.Location = new System.Drawing.Point(500, 467);
            this.R4P.Margin = new System.Windows.Forms.Padding(4);
            this.R4P.Name = "R4P";
            this.R4P.Size = new System.Drawing.Size(138, 74);
            this.R4P.TabIndex = 159;
            this.R4P.UseVisualStyleBackColor = false;
            this.R4P.Visible = false;
            // 
            // R2P
            // 
            this.R2P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.R2P.Enabled = false;
            this.R2P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.R2P.FlatAppearance.BorderSize = 10;
            this.R2P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R2P.Location = new System.Drawing.Point(500, 404);
            this.R2P.Margin = new System.Windows.Forms.Padding(4);
            this.R2P.Name = "R2P";
            this.R2P.Size = new System.Drawing.Size(138, 52);
            this.R2P.TabIndex = 157;
            this.R2P.UseVisualStyleBackColor = false;
            this.R2P.Visible = false;
            // 
            // R1P
            // 
            this.R1P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.R1P.Enabled = false;
            this.R1P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.R1P.FlatAppearance.BorderSize = 10;
            this.R1P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R1P.Location = new System.Drawing.Point(500, 347);
            this.R1P.Margin = new System.Windows.Forms.Padding(4);
            this.R1P.Name = "R1P";
            this.R1P.Size = new System.Drawing.Size(138, 52);
            this.R1P.TabIndex = 156;
            this.R1P.UseVisualStyleBackColor = false;
            this.R1P.Visible = false;
            // 
            // R11A
            // 
            this.R11A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.R11A.Enabled = false;
            this.R11A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.R11A.FlatAppearance.BorderSize = 10;
            this.R11A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R11A.Location = new System.Drawing.Point(500, 290);
            this.R11A.Margin = new System.Windows.Forms.Padding(4);
            this.R11A.Name = "R11A";
            this.R11A.Size = new System.Drawing.Size(138, 52);
            this.R11A.TabIndex = 155;
            this.R11A.UseVisualStyleBackColor = false;
            this.R11A.Visible = false;
            // 
            // R10A
            // 
            this.R10A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.R10A.Enabled = false;
            this.R10A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.R10A.FlatAppearance.BorderSize = 10;
            this.R10A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R10A.Location = new System.Drawing.Point(500, 233);
            this.R10A.Margin = new System.Windows.Forms.Padding(4);
            this.R10A.Name = "R10A";
            this.R10A.Size = new System.Drawing.Size(138, 52);
            this.R10A.TabIndex = 153;
            this.R10A.UseVisualStyleBackColor = false;
            this.R10A.Visible = false;
            // 
            // R8A
            // 
            this.R8A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.R8A.Enabled = false;
            this.R8A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.R8A.FlatAppearance.BorderSize = 10;
            this.R8A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R8A.Location = new System.Drawing.Point(500, 147);
            this.R8A.Margin = new System.Windows.Forms.Padding(4);
            this.R8A.Name = "R8A";
            this.R8A.Size = new System.Drawing.Size(138, 52);
            this.R8A.TabIndex = 151;
            this.R8A.UseVisualStyleBackColor = false;
            this.R8A.Visible = false;
            // 
            // textBox13
            // 
            this.textBox13.Enabled = false;
            this.textBox13.Location = new System.Drawing.Point(498, 146);
            this.textBox13.Margin = new System.Windows.Forms.Padding(4);
            this.textBox13.Multiline = true;
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(140, 517);
            this.textBox13.TabIndex = 150;
            // 
            // F6P
            // 
            this.F6P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F6P.Enabled = false;
            this.F6P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F6P.FlatAppearance.BorderSize = 10;
            this.F6P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F6P.Location = new System.Drawing.Point(648, 561);
            this.F6P.Margin = new System.Windows.Forms.Padding(4);
            this.F6P.Name = "F6P";
            this.F6P.Size = new System.Drawing.Size(138, 100);
            this.F6P.TabIndex = 175;
            this.F6P.UseVisualStyleBackColor = false;
            this.F6P.Visible = false;
            // 
            // F5P
            // 
            this.F5P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F5P.Enabled = false;
            this.F5P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F5P.FlatAppearance.BorderSize = 10;
            this.F5P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F5P.Location = new System.Drawing.Point(648, 508);
            this.F5P.Margin = new System.Windows.Forms.Padding(4);
            this.F5P.Name = "F5P";
            this.F5P.Size = new System.Drawing.Size(138, 35);
            this.F5P.TabIndex = 173;
            this.F5P.UseVisualStyleBackColor = false;
            this.F5P.Visible = false;
            // 
            // F4P
            // 
            this.F4P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F4P.Enabled = false;
            this.F4P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F4P.FlatAppearance.BorderSize = 10;
            this.F4P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F4P.Location = new System.Drawing.Point(648, 467);
            this.F4P.Margin = new System.Windows.Forms.Padding(4);
            this.F4P.Name = "F4P";
            this.F4P.Size = new System.Drawing.Size(138, 35);
            this.F4P.TabIndex = 172;
            this.F4P.UseVisualStyleBackColor = false;
            this.F4P.Visible = false;
            // 
            // F3P
            // 
            this.F3P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F3P.Enabled = false;
            this.F3P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F3P.FlatAppearance.BorderSize = 10;
            this.F3P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F3P.Location = new System.Drawing.Point(648, 427);
            this.F3P.Margin = new System.Windows.Forms.Padding(4);
            this.F3P.Name = "F3P";
            this.F3P.Size = new System.Drawing.Size(138, 35);
            this.F3P.TabIndex = 171;
            this.F3P.UseVisualStyleBackColor = false;
            this.F3P.Visible = false;
            // 
            // F2P
            // 
            this.F2P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F2P.Enabled = false;
            this.F2P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F2P.FlatAppearance.BorderSize = 10;
            this.F2P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F2P.Location = new System.Drawing.Point(648, 387);
            this.F2P.Margin = new System.Windows.Forms.Padding(4);
            this.F2P.Name = "F2P";
            this.F2P.Size = new System.Drawing.Size(138, 35);
            this.F2P.TabIndex = 170;
            this.F2P.UseVisualStyleBackColor = false;
            this.F2P.Visible = false;
            // 
            // F1P
            // 
            this.F1P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F1P.Enabled = false;
            this.F1P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F1P.FlatAppearance.BorderSize = 10;
            this.F1P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F1P.Location = new System.Drawing.Point(648, 347);
            this.F1P.Margin = new System.Windows.Forms.Padding(4);
            this.F1P.Name = "F1P";
            this.F1P.Size = new System.Drawing.Size(138, 35);
            this.F1P.TabIndex = 169;
            this.F1P.UseVisualStyleBackColor = false;
            this.F1P.Visible = false;
            // 
            // F12P
            // 
            this.F12P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F12P.Enabled = false;
            this.F12P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F12P.FlatAppearance.BorderSize = 10;
            this.F12P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F12P.Location = new System.Drawing.Point(648, 307);
            this.F12P.Margin = new System.Windows.Forms.Padding(4);
            this.F12P.Name = "F12P";
            this.F12P.Size = new System.Drawing.Size(138, 35);
            this.F12P.TabIndex = 168;
            this.F12P.UseVisualStyleBackColor = false;
            this.F12P.Visible = false;
            // 
            // F11A
            // 
            this.F11A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F11A.Enabled = false;
            this.F11A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F11A.FlatAppearance.BorderSize = 10;
            this.F11A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F11A.Location = new System.Drawing.Point(648, 266);
            this.F11A.Margin = new System.Windows.Forms.Padding(4);
            this.F11A.Name = "F11A";
            this.F11A.Size = new System.Drawing.Size(138, 35);
            this.F11A.TabIndex = 167;
            this.F11A.UseVisualStyleBackColor = false;
            this.F11A.Visible = false;
            // 
            // F10A
            // 
            this.F10A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F10A.Enabled = false;
            this.F10A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F10A.FlatAppearance.BorderSize = 10;
            this.F10A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F10A.Location = new System.Drawing.Point(648, 227);
            this.F10A.Margin = new System.Windows.Forms.Padding(4);
            this.F10A.Name = "F10A";
            this.F10A.Size = new System.Drawing.Size(138, 35);
            this.F10A.TabIndex = 166;
            this.F10A.UseVisualStyleBackColor = false;
            this.F10A.Visible = false;
            // 
            // F9A
            // 
            this.F9A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F9A.Enabled = false;
            this.F9A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F9A.FlatAppearance.BorderSize = 10;
            this.F9A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F9A.Location = new System.Drawing.Point(648, 187);
            this.F9A.Margin = new System.Windows.Forms.Padding(4);
            this.F9A.Name = "F9A";
            this.F9A.Size = new System.Drawing.Size(138, 35);
            this.F9A.TabIndex = 165;
            this.F9A.UseVisualStyleBackColor = false;
            this.F9A.Visible = false;
            // 
            // F8A
            // 
            this.F8A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.F8A.Enabled = false;
            this.F8A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.F8A.FlatAppearance.BorderSize = 10;
            this.F8A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.F8A.Location = new System.Drawing.Point(648, 147);
            this.F8A.Margin = new System.Windows.Forms.Padding(4);
            this.F8A.Name = "F8A";
            this.F8A.Size = new System.Drawing.Size(138, 35);
            this.F8A.TabIndex = 164;
            this.F8A.UseVisualStyleBackColor = false;
            this.F8A.Visible = false;
            // 
            // textBox14
            // 
            this.textBox14.Enabled = false;
            this.textBox14.Location = new System.Drawing.Point(646, 146);
            this.textBox14.Margin = new System.Windows.Forms.Padding(4);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(140, 517);
            this.textBox14.TabIndex = 163;
            // 
            // W6P
            // 
            this.W6P.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.W6P.Enabled = false;
            this.W6P.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.W6P.FlatAppearance.BorderSize = 10;
            this.W6P.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W6P.Location = new System.Drawing.Point(354, 561);
            this.W6P.Margin = new System.Windows.Forms.Padding(4);
            this.W6P.Name = "W6P";
            this.W6P.Size = new System.Drawing.Size(138, 100);
            this.W6P.TabIndex = 178;
            this.W6P.UseVisualStyleBackColor = false;
            this.W6P.Visible = false;
            // 
            // searchButton
            // 
            this.searchButton.BackColor = System.Drawing.Color.AliceBlue;
            this.searchButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.searchButton.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchButton.ForeColor = System.Drawing.SystemColors.InfoText;
            this.searchButton.Location = new System.Drawing.Point(1004, 15);
            this.searchButton.Margin = new System.Windows.Forms.Padding(4);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(70, 33);
            this.searchButton.TabIndex = 179;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = false;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // dayPicker
            // 
            this.dayPicker.BackColor = System.Drawing.SystemColors.Control;
            this.dayPicker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dayPicker.CheckOnClick = true;
            this.dayPicker.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dayPicker.FormattingEnabled = true;
            this.dayPicker.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday"});
            this.dayPicker.Location = new System.Drawing.Point(557, 48);
            this.dayPicker.Margin = new System.Windows.Forms.Padding(4);
            this.dayPicker.Name = "dayPicker";
            this.dayPicker.Size = new System.Drawing.Size(90, 87);
            this.dayPicker.TabIndex = 180;
            this.dayPicker.Visible = false;
            // 
            // timePicker
            // 
            this.timePicker.BackColor = System.Drawing.SystemColors.Window;
            this.timePicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timePicker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.timePicker.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePicker.ForeColor = System.Drawing.SystemColors.WindowText;
            this.timePicker.FormattingEnabled = true;
            this.timePicker.Items.AddRange(new object[] {
            "",
            "8:00",
            "9:00",
            "10:00",
            "10:05",
            "11:00",
            "11:30",
            "12:00",
            "1:00",
            "2:00",
            "2:30",
            "3:00",
            "4:00",
            "5:00",
            "6:00",
            "6:30"});
            this.timePicker.Location = new System.Drawing.Point(648, 48);
            this.timePicker.Margin = new System.Windows.Forms.Padding(4);
            this.timePicker.Name = "timePicker";
            this.timePicker.Size = new System.Drawing.Size(80, 21);
            this.timePicker.TabIndex = 181;
            this.timePicker.Visible = false;
            this.timePicker.SelectedIndexChanged += new System.EventHandler(this.timePicker_SelectedIndexChanged);
            this.timePicker.Click += new System.EventHandler(this.timePicker_Click);
            // 
            // dayTimeButton
            // 
            this.dayTimeButton.BackColor = System.Drawing.Color.White;
            this.dayTimeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dayTimeButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dayTimeButton.ForeColor = System.Drawing.Color.Silver;
            this.dayTimeButton.Location = new System.Drawing.Point(558, 15);
            this.dayTimeButton.Margin = new System.Windows.Forms.Padding(4);
            this.dayTimeButton.Name = "dayTimeButton";
            this.dayTimeButton.Size = new System.Drawing.Size(172, 33);
            this.dayTimeButton.TabIndex = 182;
            this.dayTimeButton.Text = "Day/Time";
            this.dayTimeButton.UseVisualStyleBackColor = false;
            this.dayTimeButton.Click += new System.EventHandler(this.dayTimeButton_Click);
            // 
            // M8A
            // 
            this.M8A.BackColor = System.Drawing.Color.MistyRose;
            this.M8A.Enabled = false;
            this.M8A.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.M8A.FlatAppearance.BorderSize = 10;
            this.M8A.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M8A.Location = new System.Drawing.Point(60, 147);
            this.M8A.Margin = new System.Windows.Forms.Padding(4);
            this.M8A.Name = "M8A";
            this.M8A.Size = new System.Drawing.Size(138, 35);
            this.M8A.TabIndex = 110;
            this.M8A.UseVisualStyleBackColor = false;
            this.M8A.Visible = false;
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.BackColor = System.Drawing.Color.White;
            this.departmentLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentLabel.ForeColor = System.Drawing.Color.Silver;
            this.departmentLabel.Location = new System.Drawing.Point(201, 19);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(112, 25);
            this.departmentLabel.TabIndex = 202;
            this.departmentLabel.Text = "Department";
            // 
            // courseCodeLabel
            // 
            this.courseCodeLabel.AutoSize = true;
            this.courseCodeLabel.BackColor = System.Drawing.Color.White;
            this.courseCodeLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.courseCodeLabel.ForeColor = System.Drawing.Color.Silver;
            this.courseCodeLabel.Location = new System.Drawing.Point(381, 18);
            this.courseCodeLabel.Name = "courseCodeLabel";
            this.courseCodeLabel.Size = new System.Drawing.Size(120, 25);
            this.courseCodeLabel.TabIndex = 203;
            this.courseCodeLabel.Text = "Course Code";
            // 
            // finalsButtonSmall
            // 
            this.finalsButtonSmall.AutoSize = true;
            this.finalsButtonSmall.BackColor = System.Drawing.Color.AliceBlue;
            this.finalsButtonSmall.Enabled = false;
            this.finalsButtonSmall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.finalsButtonSmall.Location = new System.Drawing.Point(47, 41);
            this.finalsButtonSmall.Margin = new System.Windows.Forms.Padding(4);
            this.finalsButtonSmall.Name = "finalsButtonSmall";
            this.finalsButtonSmall.Size = new System.Drawing.Size(12, 11);
            this.finalsButtonSmall.TabIndex = 204;
            this.finalsButtonSmall.UseVisualStyleBackColor = true;
            this.finalsButtonSmall.CheckedChanged += new System.EventHandler(this.finalsButtonSmall_CheckedChanged);
            // 
            // check
            // 
            this.check.BackColor = System.Drawing.Color.CornflowerBlue;
            this.check.Enabled = false;
            this.check.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.check.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.check.Location = new System.Drawing.Point(48, 42);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(9, 9);
            this.check.TabIndex = 205;
            this.check.UseVisualStyleBackColor = false;
            this.check.Visible = false;
            // 
            // FR7
            // 
            this.FR7.BackColor = System.Drawing.Color.Thistle;
            this.FR7.Enabled = false;
            this.FR7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FR7.FlatAppearance.BorderSize = 10;
            this.FR7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FR7.Location = new System.Drawing.Point(61, 585);
            this.FR7.Margin = new System.Windows.Forms.Padding(4);
            this.FR7.Name = "FR7";
            this.FR7.Size = new System.Drawing.Size(138, 78);
            this.FR7.TabIndex = 209;
            this.FR7.UseVisualStyleBackColor = false;
            this.FR7.Visible = false;
            // 
            // FF7
            // 
            this.FF7.BackColor = System.Drawing.Color.Thistle;
            this.FF7.Enabled = false;
            this.FF7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FF7.FlatAppearance.BorderSize = 10;
            this.FF7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FF7.Location = new System.Drawing.Point(208, 585);
            this.FF7.Margin = new System.Windows.Forms.Padding(4);
            this.FF7.Name = "FF7";
            this.FF7.Size = new System.Drawing.Size(138, 78);
            this.FF7.TabIndex = 212;
            this.FF7.UseVisualStyleBackColor = false;
            this.FF7.Visible = false;
            // 
            // FF2
            // 
            this.FF2.BackColor = System.Drawing.Color.Thistle;
            this.FF2.Enabled = false;
            this.FF2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FF2.FlatAppearance.BorderSize = 10;
            this.FF2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FF2.Location = new System.Drawing.Point(208, 389);
            this.FF2.Margin = new System.Windows.Forms.Padding(4);
            this.FF2.Name = "FF2";
            this.FF2.Size = new System.Drawing.Size(138, 89);
            this.FF2.TabIndex = 211;
            this.FF2.UseVisualStyleBackColor = false;
            this.FF2.Visible = false;
            // 
            // FF9
            // 
            this.FF9.BackColor = System.Drawing.Color.Thistle;
            this.FF9.Enabled = false;
            this.FF9.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FF9.FlatAppearance.BorderSize = 10;
            this.FF9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FF9.Location = new System.Drawing.Point(208, 189);
            this.FF9.Margin = new System.Windows.Forms.Padding(4);
            this.FF9.Name = "FF9";
            this.FF9.Size = new System.Drawing.Size(138, 89);
            this.FF9.TabIndex = 210;
            this.FF9.UseVisualStyleBackColor = false;
            this.FF9.Visible = false;
            // 
            // FS7
            // 
            this.FS7.BackColor = System.Drawing.Color.Thistle;
            this.FS7.Enabled = false;
            this.FS7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FS7.FlatAppearance.BorderSize = 10;
            this.FS7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FS7.Location = new System.Drawing.Point(354, 585);
            this.FS7.Margin = new System.Windows.Forms.Padding(4);
            this.FS7.Name = "FS7";
            this.FS7.Size = new System.Drawing.Size(138, 78);
            this.FS7.TabIndex = 215;
            this.FS7.UseVisualStyleBackColor = false;
            this.FS7.Visible = false;
            // 
            // FS2
            // 
            this.FS2.BackColor = System.Drawing.Color.Thistle;
            this.FS2.Enabled = false;
            this.FS2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FS2.FlatAppearance.BorderSize = 10;
            this.FS2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FS2.Location = new System.Drawing.Point(354, 389);
            this.FS2.Margin = new System.Windows.Forms.Padding(4);
            this.FS2.Name = "FS2";
            this.FS2.Size = new System.Drawing.Size(138, 89);
            this.FS2.TabIndex = 214;
            this.FS2.UseVisualStyleBackColor = false;
            this.FS2.Visible = false;
            // 
            // FS9
            // 
            this.FS9.BackColor = System.Drawing.Color.Thistle;
            this.FS9.Enabled = false;
            this.FS9.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FS9.FlatAppearance.BorderSize = 10;
            this.FS9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FS9.Location = new System.Drawing.Point(354, 189);
            this.FS9.Margin = new System.Windows.Forms.Padding(4);
            this.FS9.Name = "FS9";
            this.FS9.Size = new System.Drawing.Size(138, 89);
            this.FS9.TabIndex = 213;
            this.FS9.UseVisualStyleBackColor = false;
            this.FS9.Visible = false;
            // 
            // FM7
            // 
            this.FM7.BackColor = System.Drawing.Color.Thistle;
            this.FM7.Enabled = false;
            this.FM7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FM7.FlatAppearance.BorderSize = 10;
            this.FM7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FM7.Location = new System.Drawing.Point(500, 585);
            this.FM7.Margin = new System.Windows.Forms.Padding(4);
            this.FM7.Name = "FM7";
            this.FM7.Size = new System.Drawing.Size(138, 78);
            this.FM7.TabIndex = 218;
            this.FM7.UseVisualStyleBackColor = false;
            this.FM7.Visible = false;
            // 
            // FM2
            // 
            this.FM2.BackColor = System.Drawing.Color.Thistle;
            this.FM2.Enabled = false;
            this.FM2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FM2.FlatAppearance.BorderSize = 10;
            this.FM2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FM2.Location = new System.Drawing.Point(500, 389);
            this.FM2.Margin = new System.Windows.Forms.Padding(4);
            this.FM2.Name = "FM2";
            this.FM2.Size = new System.Drawing.Size(138, 89);
            this.FM2.TabIndex = 217;
            this.FM2.UseVisualStyleBackColor = false;
            this.FM2.Visible = false;
            // 
            // FM9
            // 
            this.FM9.BackColor = System.Drawing.Color.Thistle;
            this.FM9.Enabled = false;
            this.FM9.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FM9.FlatAppearance.BorderSize = 10;
            this.FM9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FM9.Location = new System.Drawing.Point(500, 189);
            this.FM9.Margin = new System.Windows.Forms.Padding(4);
            this.FM9.Name = "FM9";
            this.FM9.Size = new System.Drawing.Size(138, 89);
            this.FM9.TabIndex = 216;
            this.FM9.UseVisualStyleBackColor = false;
            this.FM9.Visible = false;
            // 
            // FT7
            // 
            this.FT7.BackColor = System.Drawing.Color.Thistle;
            this.FT7.Enabled = false;
            this.FT7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FT7.FlatAppearance.BorderSize = 10;
            this.FT7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FT7.Location = new System.Drawing.Point(648, 585);
            this.FT7.Margin = new System.Windows.Forms.Padding(4);
            this.FT7.Name = "FT7";
            this.FT7.Size = new System.Drawing.Size(138, 78);
            this.FT7.TabIndex = 221;
            this.FT7.UseVisualStyleBackColor = false;
            this.FT7.Visible = false;
            // 
            // FT2
            // 
            this.FT2.BackColor = System.Drawing.Color.Thistle;
            this.FT2.Enabled = false;
            this.FT2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FT2.FlatAppearance.BorderSize = 10;
            this.FT2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FT2.Location = new System.Drawing.Point(648, 389);
            this.FT2.Margin = new System.Windows.Forms.Padding(4);
            this.FT2.Name = "FT2";
            this.FT2.Size = new System.Drawing.Size(138, 89);
            this.FT2.TabIndex = 220;
            this.FT2.UseVisualStyleBackColor = false;
            this.FT2.Visible = false;
            // 
            // FT9
            // 
            this.FT9.BackColor = System.Drawing.Color.Thistle;
            this.FT9.Enabled = false;
            this.FT9.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FT9.FlatAppearance.BorderSize = 10;
            this.FT9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FT9.Location = new System.Drawing.Point(648, 189);
            this.FT9.Margin = new System.Windows.Forms.Padding(4);
            this.FT9.Name = "FT9";
            this.FT9.Size = new System.Drawing.Size(138, 89);
            this.FT9.TabIndex = 219;
            this.FT9.UseVisualStyleBackColor = false;
            this.FT9.Visible = false;
            // 
            // search_results_panel
            // 
            this.search_results_panel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.search_results_panel.Location = new System.Drawing.Point(815, 101);
            this.search_results_panel.Name = "search_results_panel";
            this.search_results_panel.Size = new System.Drawing.Size(267, 243);
            this.search_results_panel.TabIndex = 222;
            // 
            // added_courses_panel
            // 
            this.added_courses_panel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.added_courses_panel.Location = new System.Drawing.Point(816, 394);
            this.added_courses_panel.Name = "added_courses_panel";
            this.added_courses_panel.Size = new System.Drawing.Size(268, 276);
            this.added_courses_panel.TabIndex = 223;
            // 
            // Add_Schedule_Button
            // 
            this.Add_Schedule_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Add_Schedule_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_Schedule_Button.Location = new System.Drawing.Point(12, 71);
            this.Add_Schedule_Button.Name = "Add_Schedule_Button";
            this.Add_Schedule_Button.Size = new System.Drawing.Size(128, 23);
            this.Add_Schedule_Button.TabIndex = 195;
            this.Add_Schedule_Button.Text = "Add New Schedule";
            this.Add_Schedule_Button.UseVisualStyleBackColor = true;
            this.Add_Schedule_Button.Click += new System.EventHandler(this.Add_Schedule_Button_Click);
            // 
            // Schedule1_Button
            // 
            this.Schedule1_Button.BackColor = System.Drawing.Color.LightGray;
            this.Schedule1_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule1_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule1_Button.Location = new System.Drawing.Point(146, 68);
            this.Schedule1_Button.Name = "Schedule1_Button";
            this.Schedule1_Button.Size = new System.Drawing.Size(105, 30);
            this.Schedule1_Button.TabIndex = 196;
            this.Schedule1_Button.Text = "Schedule 1      ";
            this.Schedule1_Button.UseVisualStyleBackColor = false;
            this.Schedule1_Button.Click += new System.EventHandler(this.Schedule1_Button_Click);
            // 
            // Schedule2_Button
            // 
            this.Schedule2_Button.BackColor = System.Drawing.Color.LightGray;
            this.Schedule2_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule2_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule2_Button.Location = new System.Drawing.Point(257, 68);
            this.Schedule2_Button.Name = "Schedule2_Button";
            this.Schedule2_Button.Size = new System.Drawing.Size(105, 30);
            this.Schedule2_Button.TabIndex = 197;
            this.Schedule2_Button.Text = "Schedule 2      ";
            this.Schedule2_Button.UseVisualStyleBackColor = false;
            this.Schedule2_Button.Visible = false;
            this.Schedule2_Button.Click += new System.EventHandler(this.Schedule2_button_Click);
            // 
            // Schedule3_Button
            // 
            this.Schedule3_Button.BackColor = System.Drawing.Color.LightGray;
            this.Schedule3_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule3_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule3_Button.Location = new System.Drawing.Point(368, 68);
            this.Schedule3_Button.Name = "Schedule3_Button";
            this.Schedule3_Button.Size = new System.Drawing.Size(105, 30);
            this.Schedule3_Button.TabIndex = 198;
            this.Schedule3_Button.Text = "Schedule 3      ";
            this.Schedule3_Button.UseVisualStyleBackColor = false;
            this.Schedule3_Button.Visible = false;
            this.Schedule3_Button.Click += new System.EventHandler(this.Schedule3_Button_Click);
            // 
            // Schedule4_Button
            // 
            this.Schedule4_Button.BackColor = System.Drawing.Color.LightGray;
            this.Schedule4_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule4_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule4_Button.Location = new System.Drawing.Point(479, 68);
            this.Schedule4_Button.Name = "Schedule4_Button";
            this.Schedule4_Button.Size = new System.Drawing.Size(105, 30);
            this.Schedule4_Button.TabIndex = 199;
            this.Schedule4_Button.Text = "Schedule 4      ";
            this.Schedule4_Button.UseVisualStyleBackColor = false;
            this.Schedule4_Button.Visible = false;
            this.Schedule4_Button.Click += new System.EventHandler(this.Schedule4_Button_Click);
            // 
            // Schedule5_Button
            // 
            this.Schedule5_Button.BackColor = System.Drawing.Color.LightGray;
            this.Schedule5_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule5_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule5_Button.Location = new System.Drawing.Point(590, 68);
            this.Schedule5_Button.Name = "Schedule5_Button";
            this.Schedule5_Button.Size = new System.Drawing.Size(105, 30);
            this.Schedule5_Button.TabIndex = 200;
            this.Schedule5_Button.Text = "Schedule 5      ";
            this.Schedule5_Button.UseVisualStyleBackColor = false;
            this.Schedule5_Button.Visible = false;
            this.Schedule5_Button.Click += new System.EventHandler(this.Schedule5_Button_Click);
            // 
            // Schedule6_Button
            // 
            this.Schedule6_Button.BackColor = System.Drawing.Color.LightGray;
            this.Schedule6_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule6_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule6_Button.Location = new System.Drawing.Point(701, 68);
            this.Schedule6_Button.Name = "Schedule6_Button";
            this.Schedule6_Button.Size = new System.Drawing.Size(105, 30);
            this.Schedule6_Button.TabIndex = 201;
            this.Schedule6_Button.Text = "Schedule 6      ";
            this.Schedule6_Button.UseVisualStyleBackColor = false;
            this.Schedule6_Button.Visible = false;
            this.Schedule6_Button.Click += new System.EventHandler(this.Schedule6_Button_Click);
            // 
            // Schedule1_EXIT_Button
            // 
            this.Schedule1_EXIT_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule1_EXIT_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule1_EXIT_Button.Image = ((System.Drawing.Image)(resources.GetObject("Schedule1_EXIT_Button.Image")));
            this.Schedule1_EXIT_Button.Location = new System.Drawing.Point(224, 72);
            this.Schedule1_EXIT_Button.Name = "Schedule1_EXIT_Button";
            this.Schedule1_EXIT_Button.Size = new System.Drawing.Size(23, 23);
            this.Schedule1_EXIT_Button.TabIndex = 202;
            this.Schedule1_EXIT_Button.UseVisualStyleBackColor = true;
            this.Schedule1_EXIT_Button.Click += new System.EventHandler(this.Schedule1_EXIT_Button_Click);
            // 
            // Schedule2_EXIT_Button
            // 
            this.Schedule2_EXIT_Button.BackColor = System.Drawing.SystemColors.Control;
            this.Schedule2_EXIT_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule2_EXIT_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule2_EXIT_Button.Image = ((System.Drawing.Image)(resources.GetObject("Schedule2_EXIT_Button.Image")));
            this.Schedule2_EXIT_Button.Location = new System.Drawing.Point(335, 72);
            this.Schedule2_EXIT_Button.Name = "Schedule2_EXIT_Button";
            this.Schedule2_EXIT_Button.Size = new System.Drawing.Size(23, 23);
            this.Schedule2_EXIT_Button.TabIndex = 203;
            this.Schedule2_EXIT_Button.UseVisualStyleBackColor = false;
            this.Schedule2_EXIT_Button.Visible = false;
            this.Schedule2_EXIT_Button.Click += new System.EventHandler(this.Schedule2_EXIT_Button_Click);
            // 
            // Schedule3_EXIT_Button
            // 
            this.Schedule3_EXIT_Button.BackColor = System.Drawing.SystemColors.Control;
            this.Schedule3_EXIT_Button.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Schedule3_EXIT_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule3_EXIT_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule3_EXIT_Button.Image = ((System.Drawing.Image)(resources.GetObject("Schedule3_EXIT_Button.Image")));
            this.Schedule3_EXIT_Button.Location = new System.Drawing.Point(446, 72);
            this.Schedule3_EXIT_Button.Name = "Schedule3_EXIT_Button";
            this.Schedule3_EXIT_Button.Size = new System.Drawing.Size(23, 23);
            this.Schedule3_EXIT_Button.TabIndex = 204;
            this.Schedule3_EXIT_Button.UseVisualStyleBackColor = false;
            this.Schedule3_EXIT_Button.Visible = false;
            this.Schedule3_EXIT_Button.Click += new System.EventHandler(this.Schedule3_EXIT_Button_Click);
            // 
            // Schedule4_EXIT_Button
            // 
            this.Schedule4_EXIT_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule4_EXIT_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule4_EXIT_Button.Image = ((System.Drawing.Image)(resources.GetObject("Schedule4_EXIT_Button.Image")));
            this.Schedule4_EXIT_Button.Location = new System.Drawing.Point(557, 72);
            this.Schedule4_EXIT_Button.Name = "Schedule4_EXIT_Button";
            this.Schedule4_EXIT_Button.Size = new System.Drawing.Size(23, 23);
            this.Schedule4_EXIT_Button.TabIndex = 205;
            this.Schedule4_EXIT_Button.UseVisualStyleBackColor = true;
            this.Schedule4_EXIT_Button.Visible = false;
            this.Schedule4_EXIT_Button.Click += new System.EventHandler(this.Schedule4_EXIT_Button_Click);
            // 
            // Schedule5_EXIT_Button
            // 
            this.Schedule5_EXIT_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule5_EXIT_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule5_EXIT_Button.Image = ((System.Drawing.Image)(resources.GetObject("Schedule5_EXIT_Button.Image")));
            this.Schedule5_EXIT_Button.Location = new System.Drawing.Point(668, 72);
            this.Schedule5_EXIT_Button.Name = "Schedule5_EXIT_Button";
            this.Schedule5_EXIT_Button.Size = new System.Drawing.Size(23, 23);
            this.Schedule5_EXIT_Button.TabIndex = 206;
            this.Schedule5_EXIT_Button.UseVisualStyleBackColor = true;
            this.Schedule5_EXIT_Button.Visible = false;
            this.Schedule5_EXIT_Button.Click += new System.EventHandler(this.Schedule5_EXIT_Button_Click);
            // 
            // Schedule6_EXIT_Button
            // 
            this.Schedule6_EXIT_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Schedule6_EXIT_Button.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schedule6_EXIT_Button.Image = ((System.Drawing.Image)(resources.GetObject("Schedule6_EXIT_Button.Image")));
            this.Schedule6_EXIT_Button.Location = new System.Drawing.Point(779, 72);
            this.Schedule6_EXIT_Button.Name = "Schedule6_EXIT_Button";
            this.Schedule6_EXIT_Button.Size = new System.Drawing.Size(23, 23);
            this.Schedule6_EXIT_Button.TabIndex = 207;
            this.Schedule6_EXIT_Button.UseVisualStyleBackColor = true;
            this.Schedule6_EXIT_Button.Visible = false;
            this.Schedule6_EXIT_Button.Click += new System.EventHandler(this.Schedule6_EXIT_Button_Click);
            // 
            // removetooltip
            // 
            this.removetooltip.AutoSize = true;
            this.removetooltip.Location = new System.Drawing.Point(913, 378);
            this.removetooltip.Name = "removetooltip";
            this.removetooltip.Size = new System.Drawing.Size(85, 13);
            this.removetooltip.TabIndex = 228;
            this.removetooltip.Text = "Click to remove";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 671);
            this.Controls.Add(this.removetooltip);
            this.Controls.Add(this.dayPicker);
            this.Controls.Add(this.added_courses_panel);
            this.Controls.Add(this.search_results_panel);
            this.Controls.Add(this.FT7);
            this.Controls.Add(this.FT2);
            this.Controls.Add(this.FT9);
            this.Controls.Add(this.FM7);
            this.Controls.Add(this.FM2);
            this.Controls.Add(this.FM9);
            this.Controls.Add(this.FS7);
            this.Controls.Add(this.FS2);
            this.Controls.Add(this.FS9);
            this.Controls.Add(this.FF7);
            this.Controls.Add(this.FF2);
            this.Controls.Add(this.FF9);
            this.Controls.Add(this.FR7);
            this.Controls.Add(this.check);
            this.Controls.Add(this.finalsButtonSmall);
            this.Controls.Add(this.courseCodeLabel);
            this.Controls.Add(this.departmentLabel);
            this.Controls.Add(this.timePicker);
            this.Controls.Add(this.Schedule6_EXIT_Button);
            this.Controls.Add(this.Schedule5_EXIT_Button);
            this.Controls.Add(this.Schedule4_EXIT_Button);
            this.Controls.Add(this.Schedule3_EXIT_Button);
            this.Controls.Add(this.Schedule2_EXIT_Button);
            this.Controls.Add(this.Schedule1_EXIT_Button);
            this.Controls.Add(this.Schedule6_Button);
            this.Controls.Add(this.Schedule5_Button);
            this.Controls.Add(this.Schedule4_Button);
            this.Controls.Add(this.Schedule3_Button);
            this.Controls.Add(this.Schedule2_Button);
            this.Controls.Add(this.Schedule1_Button);
            this.Controls.Add(this.Add_Schedule_Button);
            this.Controls.Add(this.dayTimeButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.W6P);
            this.Controls.Add(this.F6P);
            this.Controls.Add(this.F5P);
            this.Controls.Add(this.F4P);
            this.Controls.Add(this.F3P);
            this.Controls.Add(this.F2P);
            this.Controls.Add(this.F1P);
            this.Controls.Add(this.F12P);
            this.Controls.Add(this.F11A);
            this.Controls.Add(this.F10A);
            this.Controls.Add(this.F9A);
            this.Controls.Add(this.F8A);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.R6P);
            this.Controls.Add(this.R4P);
            this.Controls.Add(this.R2P);
            this.Controls.Add(this.R1P);
            this.Controls.Add(this.R11A);
            this.Controls.Add(this.R10A);
            this.Controls.Add(this.R8A);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.W5P);
            this.Controls.Add(this.W4P);
            this.Controls.Add(this.W3P);
            this.Controls.Add(this.W2P);
            this.Controls.Add(this.W1P);
            this.Controls.Add(this.W12P);
            this.Controls.Add(this.W11A);
            this.Controls.Add(this.W10A);
            this.Controls.Add(this.W9A);
            this.Controls.Add(this.W8A);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.T6P);
            this.Controls.Add(this.T4P);
            this.Controls.Add(this.T2P);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.T1P);
            this.Controls.Add(this.T11A);
            this.Controls.Add(this.T10A);
            this.Controls.Add(this.T8A);
            this.Controls.Add(this.M6P);
            this.Controls.Add(this.M5P);
            this.Controls.Add(this.M4P);
            this.Controls.Add(this.M3P);
            this.Controls.Add(this.M2P);
            this.Controls.Add(this.M1P);
            this.Controls.Add(this.M12P);
            this.Controls.Add(this.M11A);
            this.Controls.Add(this.M10A);
            this.Controls.Add(this.M9A);
            this.Controls.Add(this.M8A);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.addedCoursesLabel);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.fridayLabel);
            this.Controls.Add(this.thursdayLabel);
            this.Controls.Add(this.wednesdayLabel);
            this.Controls.Add(this.tuesdayLabel);
            this.Controls.Add(this.mondayLabel);
            this.Controls.Add(this.finalsButtonBig);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.courseCodeBox);
            this.Controls.Add(this.departmentBox);
            this.Controls.Add(this.textBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "BlockIt";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox departmentBox;
        private System.Windows.Forms.ComboBox courseCodeBox;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button finalsButtonBig;
        private System.Windows.Forms.Label mondayLabel;
        private System.Windows.Forms.Label tuesdayLabel;
        private System.Windows.Forms.Label wednesdayLabel;
        private System.Windows.Forms.Label thursdayLabel;
        private System.Windows.Forms.Label fridayLabel;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label addedCoursesLabel;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Button M9A;
        private System.Windows.Forms.Button M11A;
        private System.Windows.Forms.Button M10A;
        private System.Windows.Forms.Button M3P;
        private System.Windows.Forms.Button M2P;
        private System.Windows.Forms.Button M1P;
        private System.Windows.Forms.Button M12P;
        private System.Windows.Forms.Button M6P;
        private System.Windows.Forms.Button M5P;
        private System.Windows.Forms.Button M4P;
        private System.Windows.Forms.Button T6P;
        private System.Windows.Forms.Button T4P;
        private System.Windows.Forms.Button T2P;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button T1P;
        private System.Windows.Forms.Button T11A;
        private System.Windows.Forms.Button T10A;
        private System.Windows.Forms.Button T8A;
        private System.Windows.Forms.Button W5P;
        private System.Windows.Forms.Button W4P;
        private System.Windows.Forms.Button W3P;
        private System.Windows.Forms.Button W2P;
        private System.Windows.Forms.Button W1P;
        private System.Windows.Forms.Button W12P;
        private System.Windows.Forms.Button W11A;
        private System.Windows.Forms.Button W10A;
        private System.Windows.Forms.Button W9A;
        private System.Windows.Forms.Button W8A;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button R6P;
        private System.Windows.Forms.Button R4P;
        private System.Windows.Forms.Button R2P;
        private System.Windows.Forms.Button R1P;
        private System.Windows.Forms.Button R11A;
        private System.Windows.Forms.Button R10A;
        private System.Windows.Forms.Button R8A;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Button F6P;
        private System.Windows.Forms.Button F5P;
        private System.Windows.Forms.Button F4P;
        private System.Windows.Forms.Button F3P;
        private System.Windows.Forms.Button F2P;
        private System.Windows.Forms.Button F1P;
        private System.Windows.Forms.Button F12P;
        private System.Windows.Forms.Button F11A;
        private System.Windows.Forms.Button F10A;
        private System.Windows.Forms.Button F9A;
        private System.Windows.Forms.Button F8A;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Button W6P;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.CheckedListBox dayPicker;
        private System.Windows.Forms.ComboBox timePicker;
        private System.Windows.Forms.Button dayTimeButton;
        private System.Windows.Forms.Button M8A;
        private System.Windows.Forms.Label departmentLabel;
        private System.Windows.Forms.Label courseCodeLabel;
        private System.Windows.Forms.CheckBox finalsButtonSmall;
        private System.Windows.Forms.Button check;
        private System.Windows.Forms.Button FR7;
        private System.Windows.Forms.Button FF7;
        private System.Windows.Forms.Button FF2;
        private System.Windows.Forms.Button FF9;
        private System.Windows.Forms.Button FS7;
        private System.Windows.Forms.Button FS2;
        private System.Windows.Forms.Button FS9;
        private System.Windows.Forms.Button FM7;
        private System.Windows.Forms.Button FM2;
        private System.Windows.Forms.Button FM9;
        private System.Windows.Forms.Button FT7;
        private System.Windows.Forms.Button FT2;
        private System.Windows.Forms.Button FT9;
        private System.Windows.Forms.FlowLayoutPanel search_results_panel;
        private System.Windows.Forms.FlowLayoutPanel added_courses_panel;
        private System.Windows.Forms.Button Add_Schedule_Button;
        private System.Windows.Forms.Button Schedule1_Button;
        private System.Windows.Forms.Button Schedule2_Button;
        private System.Windows.Forms.Button Schedule3_Button;
        private System.Windows.Forms.Button Schedule4_Button;
        private System.Windows.Forms.Button Schedule5_Button;
        private System.Windows.Forms.Button Schedule6_Button;
        private System.Windows.Forms.Button Schedule1_EXIT_Button;
        private System.Windows.Forms.Button Schedule2_EXIT_Button;
        private System.Windows.Forms.Button Schedule3_EXIT_Button;
        private System.Windows.Forms.Button Schedule4_EXIT_Button;
        private System.Windows.Forms.Button Schedule5_EXIT_Button;
        private System.Windows.Forms.Button Schedule6_EXIT_Button;
        private System.Windows.Forms.Label removetooltip;
    }
}


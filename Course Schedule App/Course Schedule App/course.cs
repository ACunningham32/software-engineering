﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Schedule_App
{
    class course
    {
        //The data items below will require some annoying conversion between string and integer to handle properly.
        //In particular, afternoon times are specified in 12-hour format in the CSV file, but 24 hour times are easier for calculation.
        private TimeSpan beginTS = new TimeSpan(0, 0, 0);
        public TimeSpan BeginTime
        {
            get { return beginTS; }
        }

        private TimeSpan endTS = new TimeSpan(0, 0, 0);
        public TimeSpan EndTime
        {
            get { return endTS; }
        }

        // time slot for finals schedule
        private TimeSpan finalsTS = new TimeSpan(0, 0, 0);
        public TimeSpan Finals
        {
            get { return finalsTS; }
        }

        private string finalsDay = "NA";
        public string FinalsDay
        {
            get { return finalsDay; }
        }

        private void getFinalsTimeSlot()
        {
            TimeSpan nightClass = new TimeSpan(16,0,0);
            bool MWF = isMWF(day);
            bool TR = isTR(day);
            if (beginTS < nightClass)
            {
                if (beginTS == new TimeSpan(15, 0, 0))
                {
                    if (MWF)
                    {
                        finalsTS = new TimeSpan(19, 0, 0);
                        finalsDay = "R";
                    }
                }

                else if (beginTS == new TimeSpan(9, 0, 0))
                {
                    if (MWF)
                    {
                        finalsTS = new TimeSpan(9, 0, 0);
                        finalsDay = "F";
                    }
                }

                else if (beginTS == new TimeSpan(12, 0, 0))
                {
                    if (MWF)
                    {
                        finalsTS = new TimeSpan(14, 0, 0);
                        finalsDay = "F";
                    }
                }

                else if (beginTS == new TimeSpan(14, 30, 0))
                {
                    if (TR)
                    {
                        finalsTS = new TimeSpan(19, 0, 0);
                        finalsDay = "F";
                    }
                }

                else if (beginTS == new TimeSpan(10, 0, 0))
                {
                    if (MWF)
                    {
                        finalsTS = new TimeSpan(9, 0, 0);
                        finalsDay = "S";
                    }
                }

                else if (beginTS == new TimeSpan(8, 0, 0))
                {
                    if (MWF)
                    {
                        finalsTS = new TimeSpan(14, 0, 0);
                        finalsDay = "S";
                    }

                    else if (TR)
                    {
                        finalsTS = new TimeSpan(19, 0, 0);
                        finalsDay = "M";
                    }
                }

                else if (beginTS == new TimeSpan(13, 0, 0))
                {
                    if (TR)
                    {
                        finalsTS = new TimeSpan(19, 0, 0);
                        finalsDay = "S";
                    }

                    else if (MWF)
                    {
                        finalsTS = new TimeSpan(14, 0, 0);
                        finalsDay = "M";
                    }
                }

                else if (beginTS == new TimeSpan(11, 0, 0))
                {
                    if (MWF)
                    {
                        finalsTS = new TimeSpan(9, 0, 0);
                        finalsDay = "M";
                    }
                }

                else if (beginTS == new TimeSpan(14, 0, 0))
                {
                    if (MWF)
                    {
                        finalsTS = new TimeSpan(9, 0, 0);
                        finalsDay = "T";
                    }
                }

                else if (beginTS == new TimeSpan(10, 5, 0))
                {
                    if (TR)
                    {
                        finalsTS = new TimeSpan(14, 0, 0);
                        finalsDay = "T";
                    }
                }

                else if (beginTS == new TimeSpan(11, 30, 0))
                {
                    if (TR)
                    {
                        finalsTS = new TimeSpan(19, 0, 0);
                        finalsDay = "T";
                    }
                }
            }

        }

        private bool isMWF(string d) //function says if class is MWF class
        {
            if (d.Contains('M') && d.Contains('W') ||
                d.Contains('M') && d.Contains('F') ||
                d.Contains('W') && d.Contains('F'))
            {
                return true;
            }

            else return false;
        }

        private bool isTR(string d) // function says if class is a TR class
        {
            if (d.Contains('T') && d.Contains('R'))
                return true;

            else return false;
        }

        private string beginStr = "NA";
        public string BeginString
        {
            get { return beginStr; }
        }

        private string endStr = "NA";
        public string EndString
        {
            get { return endStr; }
        }

        //The items below SHOULD be directly parseable to integers from the file. Values of NULL should be detected, and stored as a string if necessary.
        private int enrolled = 0;
        public int Enrolled
        {
            get { return enrolled; }
        }

        private string enrolledStr = "NA";
        public string EnrolledString
        {
            get { return enrolledStr; }
        }

        private int capacity = 0;
        public int Capacity
        {
            get { return capacity; }
        }

        private string capacityStr = "NA";
        public string CapacityString
        {
            get { return capacityStr; }
        }

        private int room = 0;
        public int Room
        {
            get { return room; }
        }

        private string roomStr = "NA";
        public string RoomString
        {
            get { return roomStr; }
        }
        
        public string DisplayString
        {
            get { return Department + " " + CourseNumber + " " + Day + " " + displayTime(convertTimeSpan(beginTS),convertTimeSpan(endTS)) + "\n" + LongTitle; }
        }

        //Strings will likely be sufficient for the data items below.
        private string day = "NA"; // Ex: T/R, M/W/F, etc
        public string Day
        {
            get { return day; }
        }

        private string shortTitle = "NA";
        public string ShortTitle
        {
            get { return shortTitle; }
        }

        private string longTitle = "NA";
        public string LongTitle
        {
            get { return longTitle; }
        }

        private string building = "NA";
        public string Building
        {
            get { return building; }
        }

        private string courseCodeInfo = "NA";
        public string CourseCodeInfo
        {
            get { return courseCodeInfo; }
        }

        private string courseNumber = "NA";
        public string CourseNumber
        {
            get { return courseNumber; }
        }

        private string department = "NA";
        public string Department
        {
            get { return department; }
        }

        private string section = "NA";
        public string Section
        {
            get { return section; }
        }

        private bool lab = false;
        public bool Lab
        {
            get { return lab; }
        }

        /// <summary>
        /// Builds a course from a list of strings.
        /// </summary>
        /// <param name="input"></param>
        public course(List<string> input)
        { 
            courseCodeInfo =        input[0];
            shortTitle =            input[1];
            longTitle =             input[2];
            beginStr =              input[3];
            endStr =                input[4];
            day =                   input[5];
            building =              input[6];
            roomStr =               input[7];
            enrolledStr =           input[8];
            capacityStr =           input[9];


            string[] courseCodeSplit = courseCodeInfo.Split(' ');

            if (courseCodeSplit.Length == 4 && courseCodeSplit[3] == "L")
                lab = true;

            department = courseCodeSplit[0];
            courseNumber = courseCodeSplit[1];
            section = courseCodeSplit[2];
            if (section.Contains(" "))
                section.Replace(" ", "");



            if (beginStr != "NULL")
                beginTS = TimeSpan.Parse(beginStr);
            if (endStr != "NULL")
                endTS = TimeSpan.Parse(endStr);


            getFinalsTimeSlot();

        }

        /// <summary>
        /// Checks if an individual course conflicts with another course.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool testConflict(course other)
        {
            bool conflict = false;
            bool sameDay = false;
            foreach (char c in day)
                if (other.day.Contains(c))
                    sameDay = true;
            if (sameDay)
            {
                if (beginTS == other.beginTS)
                    conflict = true;
                if (beginStr == "NULL" || endStr == "NULL" || other.beginStr == "NULL" || other.endStr == "NULL")
                    conflict = false;
            }

            return conflict;
        }

        /// <summary>
        /// Returns the 12-hour format start time of the class.
        /// </summary>
        /// <returns></returns>
        public string TwelveHourStart()
        {
            string rv;
            TimeSpan converter = BeginTime;
            TimeSpan subReference = new TimeSpan(12, 00, 00);
            if (BeginTime >= TimeSpan.Parse("13:00:00") && BeginTime < TimeSpan.Parse("19:00:00"))
            {
                converter -= subReference;
            }
            rv = converter.ToString();
            return rv;               
        }

        /// <summary>
        /// Returns the 12-hour format end time of the class.
        /// </summary>
        /// <returns></returns>
        public string TwelveHourEnd()
        {
            string rv;
            TimeSpan converter = EndTime;
            TimeSpan subReference = new TimeSpan(12, 00, 00);
            if (EndTime >= TimeSpan.Parse("13:00:00") && EndTime < TimeSpan.Parse("19:00:00"))
            {
                converter -= subReference;
            }
            rv = converter.ToString();
            return rv;
        }

        private string convertTimeSpan(TimeSpan ts)
        {
            char[] time = new char[7];

            if (ts >= new TimeSpan(12, 0, 0) && ts < new TimeSpan(13,0,0)) // 12pm
            {
                ts.ToString().CopyTo(0, time, 0, 5);
                time[5] = 'p';
                time[6] = 'm';
            }

            else if (ts > new TimeSpan(12, 0, 0)) // afternoon hours
            {
                ts = new TimeSpan(ts.Hours - 12, ts.Minutes, ts.Seconds);

                ts.ToString().CopyTo(1, time, 0, 4);
                time[4] = 'p';
                time[5] = 'm';
            }

            else if (ts < new TimeSpan(10, 0, 0)) // before 10am
            {
                ts.ToString().CopyTo(1, time, 0, 4);
                time[4] = 'a';
                time[5] = 'm';
            }
            else
            {
                ts.ToString().CopyTo(0, time, 0, 5); // 10am - 1pm
                time[5] = 'a';
                time[6] = 'm';
            }

            if (time[5] == 'm') // this gets rid of the space at the end of a time string
            {
                char[] fix = new char[6];
                for (int i = 0; i < 6; i++)
                {
                    fix[i] = time[i];
                }
                return new String(fix);
            }

            return new String(time);
        }

        private string displayTime(string bs, string es)
        {
            String time = bs + "-" + es;

            if (time.Contains("0:") && !time.Contains("10"))
                return null;

            return time;
        }
    }
}

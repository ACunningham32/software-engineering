﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Schedule_App
{
    class Tester
    {
        static void test_functionality(string[] args)
        {
            string readPath = "courses.csv";
            List<course> courses = Intake.makeCourseList(readPath);
        }

        /// <summary>
        /// Returns all courses that are in conflict. Unneeded.
        /// </summary>
        /// <param name="intake"></param>
        /// <returns></returns>
        static public List<course> detectConflicts(List<course> intake)
        {
            //List<Tuple<course, course>> conflicts = null;
            List<course> conflicts = new List<course>();
            foreach (course a in intake)
                foreach (course b in intake)
                    if (a.testConflict(b) && a != b)
                    {
                        //Tuple<course, course> conflictPair = Tuple.Create<course, course>(a, b);
                        conflicts.Add(a);
                    }
            return conflicts;
        }

        /// <summary>
        /// Returns a list of all courses that the given course conflicts with.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public List<course> detectConflicts(course a, List<course> b)
        {
            List<course> rv = new List<course>();
            foreach (course c in b)
            {
                if (c.testConflict(a))
                {
                    rv.Add(c);
                }
            }
            return rv;
        }

    }
}
